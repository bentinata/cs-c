#include <stdio.h>

int main() {
  int len, i, valid = 0;

  scanf("%d", &len);

  float arr[len];

  for (i = 0; i < len; i++) {
    scanf("%f", &arr[i]);
    // printf("%d is %s\n", (int)arr[i], (int)arr[i] % 2 == 0 ? "yes" : "no");
    // printf("%d is %s\n", (int)(((arr[i] - (int)arr[i]) * 100 ) + .5), (int)(((arr[i] - (int)arr[i]) * 100 ) + .5) % 2 == 1 ? "yes" : "no");
    if ((int)arr[i] % 2 == 0 && (int)(((arr[i] - (int)arr[i]) * 100 ) + .5) % 2 == 1) {
      valid += 1;
    }
  }

  if (valid >= 3) {
    printf("yes\n");
  }
  else {
    printf("no\n");
  }

  return 0;
}