#include <stdio.h>
#include <string.h>
#include <math.h>

int main() {
  char code[64];
  char decoded[16] = {'\0'};
  scanf("%s", code);
  // strcpy(code, "00000001");
  int len = strlen(code);
  int i;

  for (i = 0; i < len; i++) {
    if (code[i] == '1') {
      decoded[i / 8] += (int)pow(2, 7 - i % 8);
    }
  }

  // memset(decoded, '`', sizeof decoded);
  for (i = 0; i < strlen(decoded); i++) {
    decoded[i] += '`';
  }

  printf("%s\n", decoded);

  return 0;
}
