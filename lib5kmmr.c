#include "lib5kmmr.h"

int fib_n(int first, int second, int n) {
  return n == 1? first: fib_n(second, first + second, --n);
}

int fib_x(int first, int second, int x) {
  return 1 + (x <= first? 0: fib_x(second, first + second, x));
}
