#include <stdlib.h>
#include <stdio.h>

/*
 * On every _m function, matrix is assumed to be cube.
 * (sz, **ptr) signature because it was (sz, ptr[sz][sz]) before.
 * Decided not to use it since tcc doesn't support parameter VLAs.
 */

int** createm(size_t dim);
void freem(size_t dim, int **matrix);
void printm(size_t dim, int **matrix);

/*
 * Rotate the matrix by half of π.
 * Return the modified matrix pointer, so it can be chained.
 * For example, to rotate matrix by 1.5π:
 * rotm(sz, rotm(sz, rotm(sz, m)))
 */
int** rotm(size_t dim, int **matrix);

/*
 * Calculate determinant of provided matrix.
 * Support 2×2 and 3x3 matrix.
 */
float detm(size_t dim, int **matrix);
