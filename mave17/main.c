#include "lib.h"

int main() {
  size_t dim;
  int move;

  scanf("%lu", &dim);

  int **matrix = createm(dim);

  scanf("%d", &move);

  switch (move) {
    case 1:
      printm(dim, rotm(dim, rotm(dim, rotm(dim, matrix))));
      break;
    case 2:
      printm(dim, rotm(dim, matrix));
      break;
    case 3:
      printm(dim, rotm(dim, rotm(dim, matrix)));
      break;
    case 4:
      printf("%g\n", detm(dim, matrix));
      break;
  }

  /* Free your malloc! */
  freem(dim, matrix);

  return 0;
}
