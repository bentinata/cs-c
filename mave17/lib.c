#include "lib.h"

/* Simple rotation technique. */
int** rotm(size_t dim, int **matrix) {
  int i, j, last, top;
  for (i = 0; i < dim / 2; i++) {
    last = dim - 1 - i;
    for (j = i; j < last; j++) {
      top = matrix[i][j];
      matrix[i][j] = matrix[last - j][i];
      matrix[last - j][i] = matrix[last][last - j];
      matrix[last][last - j] = matrix[j][last];
      matrix[j][last] = top;
    }
  }
  return matrix;
}

float detm(size_t dim, int **matrix) {
  int i;
  float det = 0;
  if (dim == 2) {
    return matrix[0][0] * matrix[1][1] - matrix[0][1] * matrix[1][0];
  } else if (dim == 3) {
    /* Why not loop again?
     * Because the effort outweigh the outcomes.
     * This way is easier to look at, and easier to understand at.
     */
    for (i = 0; i < dim; i++) {
      det +=
        matrix[0][i] *
        matrix[1][(i + 1) % dim] *
        matrix[2][(i + 2) % dim]
      - matrix[0][i] *
        matrix[1][(i + 2) % dim] *
        matrix[2][(i + 1) % dim];
    }
    return det;
  } else {
    /* Don't care for higher order. */
    return 0;
  }
}

int** createm(size_t dim) {
  int i;
  int **matrix = malloc(dim * sizeof *matrix);
  for (i = 0; i < dim * dim; i++) {
    /* Malloc and scanf in one go! */
    if (i % dim == 0) matrix[i / dim] = malloc(dim * sizeof *matrix[i / dim]);
    scanf("%d", &matrix[i / dim][i % dim]);
  }
  return matrix;
}

void freem(size_t dim, int **matrix) {
  int i;
  for (i = 0; i < dim; i++) {
    free(matrix[i]);
  }
  free(matrix);
}

void printm(size_t dim, int **matrix) {
  int i;
  for (i = 0; i < dim * dim; i++) {
    printf("%d", matrix[i / dim][i % dim]);

    if (i % dim == dim - 1) printf("\n");
    else printf(" ");
    // Working code for CSPC which deemed EOL as wrong.
    //if (i % dim < dim - 1) printf(" ");
    //else if (i + 1 < dim * dim) printf("\n");
  }
}
