#include <stdio.h>

int main() {
  int lenone, lentwo, i;

  scanf("%d", &lenone);

  int one[lenone];

  for (i = 0; i < lenone; i++) {
    scanf("%d", &one[i]);
  }

  scanf("%d", &lentwo);

  int two[lentwo];

  for (i = 0; i < lentwo; i++) {
    scanf("%d", &two[i]);
  }

  for (i = 0, lenone = lenone < lentwo ? lenone : lentwo, lentwo = 1; i < lenone; i++) {
    if (one[i] % 2 != two[i] % 2) {
      // Reusing variable because of reasons.
      lentwo = 0;
      break;
    }
  }

  if (lentwo) {
    printf("valid\n");
  }
  else {
    printf("tidak valid\n");
  }

  return 0;
}