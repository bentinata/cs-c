#include <stdio.h>
#include <string.h>

int seq(size_t n, size_t s, void *p, int (*c)(const void *)){
  char *a = (char *)p;
  char *l = a + n * s;
  int i = 0;

  while (a < l) {
    if (c(a)) return i;

    a += s;
    i += 1;
  }

  return -1;
}


void *check(int c) {
  int compare(const void *x) {
    int v = *((int *)x);

    if (v == c) return 1;

    return 0;
  }

  return *compare;
}

int main() {
  int r[] = {1, 2, 3, 4, 5};

  int b = check(3)(*r);


  //int a = seq(sizeof r / sizeof *r, sizeof *r, r, check(3));
  //printf("%d %d", r[a], a);
  printf("%d", b);

  return 0;
}

