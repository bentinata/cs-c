#include <stdio.h>
#include <string.h>
#define rl(x) sizeof(x)/sizeof(x[0])

void fill(size_t s, char *x, char *d) {
  size_t i;
  int a = *(const int*)x;
  //printf("filling %d...\n", a);

  for (i = 0; i < s; ++i, ++x, ++d) {
    *d = *x;
  }
}

void swap(size_t s, char *l, char *r) {
  size_t i;
  char t;

  for (i = 0; i < s; ++i, ++l, ++r) {
    t = *l;
    *l = *r;
    *r = t;
  }
}

void sort_insertion(
  size_t s,
  size_t n,
  void *p,
  int (*c)(const void *, const void *)) {
  char *l, *r, *a = (char *)p;

  for (l = a; l < a + n * s; l += s)
    for (r = l; r > a && c(r - s, r) > 0; r -= s) swap(s, r, r - s);
}


void merge(
  size_t s,
  size_t n1,
  void *p1,
  size_t n2,
  void *p2,
  void *px,
  int (*c)(const void *, const void *)) {
  char *l1, *l2, *r1, *r2, *a1 = (char *)p1, *a2 = (char *)p2;
  char *ax = (char *)px;

  l1 = a1;
  r1 = a1 + n1 * s;
  l2 = a2;
  r2 = a2 + n2 * s;

  sort_insertion(s, n2, p2, c);
  sort_insertion(s, n1, p1, c);

  //printf("\nall out\n");
  //while(l1 < a1 + n1 * s) {
  //  int x = *(int *)l1;
  //  printf("%d\n", x);
  //  l1 += s;
  //}

  //while(l2 < a2 + n2 * s) {
  //  int x = *(int *)l2;
  //  printf("%d\n", x);
  //  l2 += s;
  //}
  //printf("\n");

  while (l1 < r1 && l2 < r2) {
    if (c(l1, l2) < 0) {
      fill(s, l1, ax);
      l1 += s;
    } else {
      fill(s, l2, ax);
      l2 += s;
    }
    ax += s;
  }

  if (l1 < r1)
    fill(r1 - l1, l1, ax);
  else
    fill(r2 - l2, l2, ax);

}

int comp_int_a(const void *_a, const void *_b) {
  int a = *(const int*)_a;
  int b = *(const int*)_b;
  //printf("comparing %d and %d\n", a, b);
  return (a > b) - (a < b);
}

int comp_int_d(const void *_a, const void *_b) {
  int a = *(const int*)_a;
  int b = *(const int*)_b;
  //printf("comparing %d and %d\n", a, b);
  return (a < b) - (a > b);
}

int comp_str_a(const void *_a, const void *_b) {
  char *a = (char*)_a;
  char *b = (char*)_b;
  //return (a < b) - (a > b);
  printf("Comparing: %s %s\n", a, b);
  return strcmp(a, b);
}

int mainx() {
  int r1[] = { 1, 3, 4, 6 };
  int r2[] = { 2, 5, 7, 8 };
  int rx[rl(r1) + rl(r2)];

  merge(sizeof(rx[0]), rl(r1), r1, rl(r2), r2, rx, comp_int_d);

  for (int i = 0; i < rl(rx); i++) {
    printf("%d\n", rx[i]);
  }

  return 0;
}

int main() {
  int i;
  char r1[][8] = { "Mar", "Ben", "Zod", "Fal", "Ki", "Gung", "Fik" };
  char r2[][8] = { "Han", "Man", "Mad", "Ken", "Buy", "Vic", "Bim" };
  //printf("%d\n", rl(r1) + rl(r2));
  char rx[rl(r1) + rl(r2)][rl(r1[0])];

  merge(sizeof(rx[0]), rl(r1), r1, rl(r2), r2, rx, comp_str_a);

  for (i = 0; i < rl(rx); i++)
    printf("At index %d: %s\n", i, rx[i]);

  return 0;
}
