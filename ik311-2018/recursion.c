#include <stdio.h>

void atoz(char c) {
  printf("%c ", c);
  if (c < 'z') atoz(++c);
}

void ztoa(char c) {
  printf("%c ", c);
  if (c > 'a') ztoa(--c);
}

void ztoa2(char c) {
  if (c < 'z') ztoa2(++c);
  printf("%c ", c);
}

int fib_n(int first, int second, int n) {
  return n == 1? first: fib_n(second, first + second, --n);
}

int fib_n_even(int first, int second, int n, int carry) {
  printf("%d %d %d %d\n", first, second, n, carry);
  if (n == 1)
    return first;
  else if ((first + second) % 2)
      return fib_n_even(second, first + second, --n, carry);
  else if (carry)
    return fib_n_even(carry, first + second, --n, 0);
  else
    return fib_n_even(second, first + second, --n, first + second);
}

int fib_n_1_1(n) {
  return fib_n_even(1,3, n, 0);
}

int main() {
  printf("%d\n", fib_n_1_1(7));

  return 0;
}
