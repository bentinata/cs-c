#include <stdio.h>
#include <math.h>

int afterpoint(double value, int len) {
  return (int)((value - (int)value) * pow(10, len));
}

int main() {
  float in[6];
  int i, valid;

  for (i = 0; i < 6; i++) {
    scanf("%f", &in[i]);
    if (afterpoint(in[i], 1) % 2 == 1) {
      valid++;
    }
  }

  if (valid >= 3) {
    printf("valid\n");
  }
  else {
    printf("tidak valid\n");
  }
  return 0;
}