////////////////////////////////////////////////////////////////////////////////
// Saya, Benget Nata, bersumpah untuk tidak melakukan kecurangan, yaitu:      //
// 1. Mengetikkan kode program dengan melihat kode program teman.             //
// 2. Mengetikkan kode program berdasarkan petunjuk oleh teman.               //
// 3. Mengumpulkan kode program milik teman.                                  //
// 4. Memberikan instruksi untuk mengetikkan kode program.                    //
// 5. Memiliki alur program yang sama persis dengan milik teman.              //
// Jika saya melakukan kecurangan maka Tuhan adalah saksi saya,               //
// dan saya bersedia menerima hukuman-Nya.                                    //
////////////////////////////////////////////////////////////////////////////////

/**
 * SWEETWARE LICENSE v0.1.1:
 * As long as you retain this notice you can do
 * whatever you want with this stuff.
 * If we meet some day, and you think this stuff is worth it,
 * you can buy me sweets in return.  Benget Nata
 */

#include <stdio.h>

int main() {
  int B,
      e,
      n,
      N;

  scanf("%d", &n);
  // n = 4;

  /* First n-row output. */
  for (B = 0; B < n; B++) {
    /* There's a whitespace here... */
    printf(" ");

    for (e = n - B; e > 0; printf(" ", --e)) {}
    for (e = 0; e < 2 * (B + 1); printf("*", ++e)) {}

    /*
      (n / 2) * 10
      Explanation:
      Since the example shows that input value '1' have no gap,
      while input '3' and '5' have a gap.
      Input '3' and '5' have a '10' and '20' gap, respectively.
      I conclude that gap is half of value (rounded down), times 5.
      Since half of one rounded down is 0,
      input value '1' would've no gap.
      Thus would output as expected.
    */
    for (e = 0; e < (n / 2) * 10 - B * 2; printf(" ", ++e)) {}
    for (e = 0; e < 2 * (B + 1); printf("*", ++e)) {}
    printf("\n");
  }

  /* Second n-row output. */
  for (B = 0; B < n; B++) {
    for (e = 0; e < n; printf("*", ++e)) {}
    for (e = 0; e < n; printf(" ", ++e)) {}
    for (e = 0; e < n * 4 - (n/2) * 2; printf("*", ++e)) {}
    for (e = 0; e < n; printf(" ", ++e)) {}
    for (e = 0; e < n; printf("*", ++e)) {}
    printf("\n");
  }

  /* Third n-row output. */
  for (B = 0; B < n; B++) {
    for (e = 0; e < B; printf(" ", ++e)) {}
    for (e = 0; e < 3 * n + 1 - B; printf("*", ++e)) {}
    for (e = 0; e < (n / 2) * 2; printf(" ", ++e)) {}
    for (e = 0; e < 3 * n + 1 - B; printf("*", ++e)) {}
    printf("\n");
  }

  /* Fourth n-row output. */
  for (B = 0; B < n; B++) {
    for (e = 0; e < n * 2.5 + B; printf(" ", ++e)) {}
    for (e = 0; e < n * 2 - B * 2; printf("*", ++e)) {}
    printf("\n");
  }

  return 0;
}

/*
3
    **          **
   ****        ****
  ******      ******
***   **********   ***
***   **********   ***
***   **********   ***
**********  **********
 *********  *********
  ********  ********
        ******
         ****
          **

5
      **                    **
     ****                  ****
    ******                ******
   ********              ********
  **********            **********
*****     ****************     *****
*****     ****************     *****
*****     ****************     *****
*****     ****************     *****
*****     ****************     *****
****************    ****************
 ***************    ***************
  **************    **************
   *************    *************
    ************    ************
            ************
             **********
              ********
               ******
                ****

1
  ****
* **** *
********
   **
 */
