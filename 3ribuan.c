#include <stdio.h>

int main() {
  int i, valid, in[6];

  for (i = 0; i < 6; i++) {
    scanf("%d", &in[i]);
    if (in[i] >= 1000) {
      valid++;
    }
  }

  if (valid >= 3) {
    printf("ribuan 3 atau lebih\n");
  }
  else {
    printf("tidak valid\n");
  }
  return 0;
}