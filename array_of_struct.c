#ifdef WINDOWS
#define CLEAR system("cls");
#else
#define CLEAR write(1,"\E[H\E[2J",7);
#endif

#define arrlen(x) (sizeof x/sizeof x[0])

#include <stdio.h>

int main() {
  int board[3][3] = {0};
  int i, l;

  int w = arrlen(board), h = arrlen(board[0]);

  for (i = 0, l = (2*w+1)*(2*h+1); i < l; i++) {
    if (i == 0) {
      printf("┌");
    }
    else if (i < (w - 1) * 2 && i % 2 == 0) {
      printf("┬");
    }
    else if (i == (w - 1) * 2) {
      printf("┐");
    }
    else if (i == w * (h - 1)) {
      printf("└");
    }
    else if (i == l - 1) {
      printf("┘");
    }
    else {
      printf(" ");
    }
    // printf("%2d", i);

    if ((i + 1) % 2*w+1 == 0) {
      printf("\n");
    }
  }

  return 0;
}
