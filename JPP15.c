////////////////////////////////////////////////////////////////////////////////
// Saya Benget Nata bersumpah tidak melakukan kecurangan, yaitu:              //
// 1. Mengetikkan kode program dengan melihat kode program teman.             //
// 2. Mengetikkan kode program berdasarkan petunjuk oleh teman.               //
// 3. Mengumpulkan kode program milik teman.                                  //
// 4. Memberikan mengetikkan terkait kode program yang dikumpulkan.           //
// 5. Memiliki alur program yang sama persis dengan teman.                    //
// Jika saya melakukan kecurangan maka Tuhan adalah saksi saya,               //
// dan saya bersedia menerima hukuman-Nya.                                    //
////////////////////////////////////////////////////////////////////////////////

/** SWEETWARE LICENSE version 0.1.1
  * As long as you retain this notice
  * you can do whatever you want with this stuff.
  * If we meet some day, and you think this stuff is worth it,
  * you can buy me sweets in return.  Benget Nata
  */

#include <stdio.h>

int main() {

  int day,
      in,
      out,
      moni,
      starting_hourly,
      continous_hourly;
  float tax;
  char packet;

  scanf("%d", &day);

  // Normal day: 1, 2, 3, 4, 5. Tax = 100%.
  // Weekend day: 6, 7. Tax = 125%.
  if (day <= 5) {
    tax = 1;
  }
  else if (day > 5) {
    tax = 1.25;
  }

  scanf(" %c", &packet);
  scanf("%d", &in);
  scanf("%d", &out);

  // If in hour is bigger than out hour (e.g. in = 22, out = 4),
  // out hour is added by 24 (e.g. in = 22, out = 28).
  // Therefore, the difference is still positive number.
  if (in > out) {
    // Reusing variables so compiled program is smaller.
    in = (out + 24) - in;
  }
  // Explicit condition is a good practice.
  else if (in <= out) {
    in = out - in;
  }

  scanf("%d", &moni);

  if (packet == 'a') {
    out = 2;
    starting_hourly = 1500;
    continous_hourly = 500;
  }
  else if (packet == 'b') {
    out = 1;
    starting_hourly = 3000;
    continous_hourly = 1000;
  }


  if (in > out) {
    moni -= ((out) * starting_hourly + (in - out) * continous_hourly) * tax;
  }
  else if (in <= out) {
    moni -= starting_hourly * in * tax;
  }

  // I've got no strings.
  //        ___+___
  //
  //           o
  //         _/|\/
  //          /\
  // ejm96   / /

  if (day == 1) {
    printf("senin ");
  }
  else if (day == 2) {
    printf("selasa ");
  }
  else if (day == 3) {
    printf("rabu ");
  }
  else if (day == 4) {
    printf("kamis ");
  }
  else if (day == 5) {
    printf("jumat ");
  }
  else if (day == 6) {
    printf("sabtu ");
  }
  else if (day == 7) {
    printf("minggu ");
  }

  if (packet == 'a') {
    printf("kelabu\n");
  }
  else if (packet == 'b') {
    printf("ceria\n");
  }

  if (moni >= 0) {
    printf("%d\n", moni);
  } else if (moni < 0) {
    printf("tidak valid\n");
  }

  return 0;
}