#include <stdio.h>
#include <string.h>

unsigned int toBinary(unsigned int k) {
  return (k == 0 || k == 1? k: ((k % 2) + 10 * toBinary(k / 2)));
}

int main() {
  char code[16];
  scanf("%s", code);
  // strcpy(code, "aku");
  int i;

  for (i = 0; i < strlen(code); i++) {
    code[i] -= '`';
    printf("%08d", toBinary(code[i]));
  }

  /* They said we need a newline. */
  printf("\n");

  return 0;
}
