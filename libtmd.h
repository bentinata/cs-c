#include <string.h>
#include <stdio.h>

/**
 * Convert string into corresponding string index.
 * If not found, return -1.
 * @param  word String to be compared
 * @param  map  Array of string
 * @return      Index compared string inside array
 */
int tmd_conv(char *word, char *map[]);

/**
 * Print array of index to follow TMD spec.
 * Basically just reverse tmd_conv() process.
 * @param idx List of index string.
 * @param map Array filled with string.
 */
void tmd_words(int idx[], char *map[]);

/**
 * Print symbol that defined in TMD spec.
 * Map is binary representation of symbol.
 * @param idx   List of index string.
 * @param thick The thickness of printed symbol.
 * @param map   Array filled with predefined symbol.
 */
void tmd_symbs(int idx[], int thick, int map[]);
