////////////////////////////////////////////////////////////////////////////////
// Saya, Benget Nata, bersumpah untuk tidak melakukan kecurangan, yaitu:      //
// 1. Mentikkan kode program dengan melihat kode program teman.               //
// 2. Mentikkan kode program berdasarkan petunjuk oleh teman.                 //
// 3. Mengumpulkan kode program milik teman.                                  //
// 4. Memberikan instruksi untuk mengetikkan kode program.                    //
// 5. Memiliki alur program yang sama persis dengan milik teman.              //
// Jika saya melakukan kecurangan maka Tuhan adalah saksi saya,               //
// dan saya bersedia menerima hukuman-Nya.                                    //
////////////////////////////////////////////////////////////////////////////////

/**
 * SWEETWARE LICENSE v0.1.1:
 * As long as you retain this notice you can do
 * whatever you want with this stuff.
 * If we meet some day, and you think this stuff is worth it,
 * you can buy me sweets in return.  Benget Nata
 */

#include <stdio.h>
#include "lib5kmmr.h"

int main() {
  int width, length;
  int i;

  // All-purpose temporary variable
  // just think it's like usable /dev/null
  int trash;

  scanf("%d", &width);
  scanf("%d", &length);
  length *= width;

  int matrix_one[length];
  for (i = 0; i < length; scanf("%d", &matrix_one[i++])) {}
  
  // Actually we only need two one-dimensional array variable.
  int matrix_two[length];
  for (i = 0; i < length; scanf("%d", &matrix_two[i++])) {}

  for (i = 0; i < length; i++) {
    scanf("%d", &trash);

    // Just overwrite any of the array index value,
    // since we don't use that anymore.
    matrix_one[i] = fib_x(matrix_one[i], matrix_two[i], trash);
  }

  for (i = 0; i < length;) {
    scanf("%d", &trash);
   
    trash > matrix_one[i]? putchar('X'): putchar('O');

    // Any value larger than 0 evaluate to true.
    // Since we iterate from 0
    // we must +1 the iterator value.
    // We could use this to eliminate for post-condition parameter.
    ++i % width? putchar(' '): putchar('\n');
  }

  return 0;
}
