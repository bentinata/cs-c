#include <stdio.h>

int main() {
  int x;
  scanf("%d", &x);

  printf("%d\n", recursive_sequence(x));

  return 0;
}

int recursive_sequence(int n) {
  if (n) return recursive_sequence(n - 1) + n;
  return 0;
}

int recursive_factorial(int n) {
  if (n) return recursive_factorial(n - 1) * n;
  return 1;
}

int for_loop_factorial(int n) {
  int sum, i;
  for (sum = 1, i = 0; i < n; i++, sum *= i) {}
  return sum;
}
