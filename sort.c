#include <stdio.h>

void swap(size_t s, char *l, char *r) {
  size_t i;
  char t;

  for (i = 0; i < s; ++i, ++l, ++r) {
    t = *l;
    *l = *r;
    *r = t;
  }
}

void fill(size_t s, char *x, char *d) {
  size_t i;

  for (i = 0; i < s; ++i, ++x, ++d) {
    *d = *x;
  }
}

void bub(size_t n, size_t s, void *p, int (*c)(const void *, const void *)) {
  char *l, *x, *r, *a = (char *)p;

  for (r = a + n * s; r > a; r -= s) {
    l = a;
    x = l + s;

    while (x < r) {
      if (c(l, x) > 0) {
        swap(s, l, x);
      } else {
        l += s;
        x += s;
      }
    }
  }
}

void ins(size_t n, size_t s, void *p, int (*c)(const void *, const void *)) {
  size_t i;
  char *r, t[s], *a = (char *)p;

  for (r = a + n * s; a < r; a += s) {
    fill(s, a, t);

    while (c(a, a + s)) {
      fill(s, a, a - s);
      a -= s;
    }

    fill(s, t, a);

  }
}

// struct data {
//   int value;
// };
//
// int compare(const void *a, const void *b) {
//   //int x = ((struct data *)a)->value;
//   //int y = ((struct data *)b)->value;
//   int x = *((int *)a);
//   int y = *((int *)b);
//
//   printf("checking %d and %d\n", x, y);
//   return x > y;
// }
//
// int main() {
//   //struct data arr[5] = {{2}, {4}, {5}, {1}, {3}};
//   //struct data ar2[5] = {{2}, {4}, {5}, {1}, {3}};
//   // int arr[5] = {2, 4, 5, 1, 3};
//   int arr[] = {6, 5, 3, 1, 8, 7, 2, 4};
//   size_t s = sizeof arr / sizeof *arr;
//
//   bub(s, sizeof arr[0], arr, *compare);
//
//   printf("\narray now is: ");
//   for (int i = 0; i < s; i++) {
//     printf("%d", arr[i]);
//   }
//   printf("\n");
//
//   return 0;
// }
