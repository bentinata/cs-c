#include <stdio.h>
#include <ctype.h>

int isvocal(char c) {
  c = tolower(c);
  return (c == 'a' || c == 'e' || c == 'i' || c == 'o' || c == 'u');
}

int main() {
  char in[6];
  int valid, i = 0, vocal = 0;

  // Hard-code loop limit is faster than caching array length, smh.
  for (i = 0; i < 6; i++) {
    scanf(" %c", &in[i]);

    if (i > 0) {
      // Do relative checking 'cause it's more challenging
      if (isvocal(in[i-vocal])) {
        valid++;
      }
      else {
        valid = -6;
      }
    }

    if (isvocal(in[i])) {
      vocal = 1;
    }
    else {
      vocal = 0;
    }
  }

  if (valid > 0) {
    printf("kombinasi valid\n");
  }
  else {
    printf("kombinasi tidak valid\n");
  }

  return 0;
}