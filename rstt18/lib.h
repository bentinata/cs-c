///////////////////////////////////////////////////////////////////
// Saya Benget Nata bersumpah tidak melakukan kecurangan, yaitu: //
// 1. Mengetikkan kode program dengan melihat kode program teman //
//    dalam kegiatan yang sama.                                  //
// 2. Mengetikkan kode program berdasarkan petunjuk oleh teman   //
//    dalam kegiatan yang sama.                                  //
// 3. Mengumpulkan kode program milik teman                      //
//    dalam kegiatan yang sama.                                  //
// 4. Memberikan instruksi untuk mengetikkan kode program        //
//    terkait kode program yang dikumpulkan dalam kegiatan.      //
// 5. Memiliki alur program yang sama persis dengan teman        //
//    dalam kegiatan yang sama.                                  //
//                                                               //
// Jika saya melakukan kecurangan maka Tuhan adalah saksi saya,  //
// dan saya bersedia menerima hukuman-Nya.                       //
///////////////////////////////////////////////////////////////////

#include <stdio.h>

int recur(int n);
