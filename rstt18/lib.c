#include "lib.h"

// Calculate how many times it match the odd conditional
int recur(int n) {
  // We've reach 0
  // Recursion stack will unwrap and add 1 for every odd recursion
  if (n < 1)
    return 0;
  else if (n % 2)
    return recur(n - 1);
  else
    return 1 + recur(n / 2);
}
