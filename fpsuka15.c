////////////////////////////////////////////////////////////////////////////////
// Saya, Benget Nata, bersumpah untuk tidak melakukan kecurangan, yaitu:      //
// 1. Mengetikkan kode program dengan melihat kode program teman.             //
// 2. Mengetikkan kode program berdasarkan petunjuk oleh teman.               //
// 3. Mengumpulkan kode program milik teman.                                  //
// 4. Memberikan instruksi untuk mengetikkan kode program.                    //
// 5. Memiliki alur program yang sama persis dengan milik teman.              //
// Jika saya melakukan kecurangan maka Tuhan adalah saksi saya,               //
// dan saya bersedia menerima hukuman-Nya.                                    //
////////////////////////////////////////////////////////////////////////////////

/* SWEETWARE LICENSE v0.1.1:
 * As long as you retain this notice you can do
 * whatever you want with this stuff.
 * If we meet some day, and you think this stuff is worth it,
 * you can buy me sweets in return.  Benget Nata
 */

#include "libfpsuka.h"

int main() {
  int len;
  int i;


  scanf("%d", &len);
  char words[len][16];

  for (i = 0; i < len; i++) {
    scanf("%s", words[i]);
  }

  // for (i = 0; i < len; printf("%s\n", words[i++])) {}

  suka_print(words, len);

  return 0; 
}

// Contoh Masukan
// 4
// akuu
// adalah
// anak
// gembala


// Contoh Keluaran

// uuka
//     halada
//        kana
// alabmeg


    
// Contoh Masukan 2
// 6
// aku      3
// anak     4
// adalah   6
// gembala  7
// selaluu  7
// riang    5


// Contoh Keluaran 2

// a
//  k
//   u
//    anak
//        a
//         d
//          a
//           l
//            a
//             h
//              gembala
//                     s
//                      e
//                       l
//                        a
//                         l
//                          u
//                           u
//                            riang