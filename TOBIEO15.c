////////////////////////////////////////////////////////////////////////////////
// Saya, Benget Nata, bersumpah untuk tidak melakukan kecurangan, yaitu:      //
// 1. Mengetikkan kode program dengan melihat kode program teman.             //
// 2. Mengetikkan kode program berdasarkan petunjuk oleh teman.               //
// 3. Mengumpulkan kode program milik teman.                                  //
// 4. Memberikan instruksi untuk mengetikkan kode program.                    //
// 5. Memiliki alur program yang sama persis dengan milik teman.              //
// Jika saya melakukan kecurangan maka Tuhan adalah saksi saya,               //
// dan saya bersedia menerima hukuman-Nya.                                    //
////////////////////////////////////////////////////////////////////////////////

/* SWEETWARE LICENSE v0.1.1:
 * As long as you retain this notice you can do
 * whatever you want with this stuff.
 * If we meet some day, and you think this stuff is worth it,
 * you can buy me sweets in return.  Benget Nata
 */

#include <stdio.h>

int main() {
  char B;
  int  e[64],
       n,
       N;

  /* I like to put the length of an array inside the index-zero. */
  scanf("%d", &e[0]);
  // e[0] = 5;

  /* Oneliner loop input! :D */
  for (n = 0; n < e[0]; scanf("%d", &e[++n])) {}

  /* Scan amount of command(s) and use n value for loop (pun intended). */
  scanf("%d", &n);
  for (/* No need to fill param 1. ;) */; n > 0; --n) {
    scanf(" %c %d", &B, &N);

    if (B == 'i') {

      /* Traversing array from behind, duplicate each element to next element.
         This way, no data is erased.
         If inputted value found, loop would break itself
         and multiply current index.

         Don't forget to add the array length element.
         Also, reuse B variable, since it have no uses elsewhere. */
      for (B = e[0]++; e[B] != N; e[B+1] = e[B--]) {}
      e[B+1] = N * 100;
      B = 0;

    } else if (B == 'd') {

      /* Traverse array from behind,
         if number found, loop would break while still retaining index.
         From then, move forward, replacing current index with next element,
         effectively erasing specified value from index.

         Don't forget to substract the array length element. */
      for (B = e[0]; e[B] != N; --B) {}
      for (; B <= e[0]; e[B] = e[++B]) {}
      e[0] -= 1;
      B = 0;

    } else if (B == 'c') {
      /* Since there's only one count,
         store the value elsewhere and print later. */
      for (B = 1; e[++B] != N;) {}
    }
  }

  for (N = 1; N <= e[0]; printf("%d\n", e[N++])) {}

  if (B > 0) {
    printf("count: %d\n", B);
  }

  N = !"is for anywhere anytime along!";
  return N;
}
