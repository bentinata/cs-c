#include <stdio.h>
#include "libudn.h"

void sort_insertion(nilai *l, int n) {
  int i; // for iterator
  int c; // for checker
  nilai t; // for temporary

  // diligent people start from 0
  // efficient sorter start from 1
  // 'cause they don't bother comparing same index
  for (i = 1; i < n; i++) {
    for (c = 0; c < i; c++) {
      if (l[i].nilai > l[c].nilai) {
        // simple swippy-swappy
        t = l[i];
        l[i] = l[c];
        l[c] = t;
      }
    }
  }

  for (i = 0; i < n; i++) {
    // just shove print function here
    // no need to pollute main code with unecessary calls
    printf("%s %s %d\n", l[i].nama, l[i].kelas, l[i].nilai);
  }

}
