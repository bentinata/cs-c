#include <stdio.h>

typedef struct {
  char name[32];
  int poin;
} Act;

void swap(size_t s, char *l, char *r);
void sort_bubble(
  size_t n,
  size_t s,
  void *p,
  int (*c)(const void *, const void *));

void sort_insertion(
  size_t n,
  size_t s,
  void *p,
  int (*c)(const void *, const void *));

void sort_selection(
  size_t n,
  size_t s,
  void *p,
  int (*c)(const void *, const void *));


int comp_act_a(const void *_a, const void *_b);

int comp_act_d(const void *_a, const void *_b);
