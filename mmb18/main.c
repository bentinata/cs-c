#include "lib.h"

int main() {
  int i, n;
  char sort;

  scanf("%d", &n);
  Act academic[n];

  for (i = 0; i < n; i++)
    scanf(" %s %d", academic[i].name, &academic[i].poin);

  scanf(" %c", &sort);

  if (sort == 'A' || sort == 'a')
    sort_bubble(n, sizeof(Act), academic, comp_act_a);
  else if (sort == 'D' || sort == 'd')
    sort_bubble(n, sizeof(Act), academic, comp_act_d);

  printf("========AKADEMIK========\n");
  for (i = 0; i < n; i++)
    printf("%d. %s %d\n", i + 1, academic[i].name, academic[i].poin);


  scanf("%d", &n);
  Act nonacademic[n];

  for (i = 0; i < n; i++)
    scanf(" %s %d", nonacademic[i].name, &nonacademic[i].poin);

  scanf(" %c", &sort);

  if (sort == 'A' || sort == 'a')
    sort_bubble(n, sizeof(Act), nonacademic, comp_act_a);
  else if (sort == 'D' || sort == 'd')
    sort_bubble(n, sizeof(Act), nonacademic, comp_act_d);

  printf("\n======NON-AKADEMIK======\n");
  for (i = 0; i < n; i++)
    printf("%d. %s %d\n", i + 1, nonacademic[i].name, nonacademic[i].poin);

  return 0;
}
