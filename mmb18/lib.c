#include "lib.h"

void swap(size_t s, char *l, char *r) {
  size_t i;
  char t;

  for (i = 0; i < s; ++i, ++l, ++r) {
    t = *l;
    *l = *r;
    *r = t;
  }
}

void sort_bubble(
  size_t n,
  size_t s,
  void *p,
  int (*c)(const void *, const void *)) {
  char *l, *r, *a = (char *)p;

  for (r = a + n * s; r > a; r -= s)
    for (l = a; l + s < r; l += s) if (c(l, l + s) > 0) swap(s, l, l + s);
}

void sort_insertion(
  size_t n,
  size_t s,
  void *p,
  int (*c)(const void *, const void *)) {
  char *l, *r, *a = (char *)p;

  for (l = a; l < a + n * s; l += s)
    for (r = l; r > a && c(r - s, r) > 0; r -= s) swap(s, r, r - s);
}

void sort_selection(
  size_t n,
  size_t s,
  void *p,
  int (*c)(const void *, const void *)) {
  char *l, *r, *t, *a = (char *)p;

  for (l = a; l < a + n * s; l += s) {
    for (t = l, r = l + s; r < a + n * s; r += s) if (c(t, r) > 0) t = r;
    if (t != l) swap(s, l, t);
  }
}

int comp_act_a(const void *_a, const void *_b) {
  Act a = *(const Act*)_a;
  Act b = *(const Act*)_b;
  //printf("%d %d\n", a.poin, b.poin);
  return (a.poin > b.poin) - (a.poin < b.poin);
  //if (a < b) return -1;
  //if (a > b) return 1;
  //return 0;
}

int comp_act_d(const void *_a, const void *_b) {
  Act a = *(const Act*)_a;
  Act b = *(const Act*)_b;
  //printf("%d %d\n", a.poin, b.poin);
  return (a.poin < b.poin) - (a.poin > b.poin);
  //if (a < b) return 1;
  //if (a > b) return -1;
  //return 0;
}

