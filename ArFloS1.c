#include <stdio.h>

int main() {
  int len, i, valid = 0;
  int before;
  int after;
  float input;
  float holder;

  scanf("%d", &len);

  for (i = 0; i < len; i++) {
    scanf("%f", &input);
    before = input;
    after = (((input - (int)input) * 100) + .5);
    holder = (float)before / (float)after;
    if (holder == floor(holder)) {
      valid += 1;
    }
  }

  if (valid >= 3) {
    printf("yes\n");
  }
  else {
    printf("no\n");
  }

  return 0;
}
