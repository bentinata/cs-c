#include "libfpsuka.h"

void suka_reverseprint(char words[], int gap) {
  int len;

  printf("%*c", gap, 0);
  for (len = strlen(words); len >= 0; len--) {
    printf("%c", words[len]);
  }
  printf("\n");
}

void suka_print(char words[][16], int len) {
  /* Multiple i's allowed. */
  int i, ii;
  int wlen;
  int gap = 0;

  // printf("%d\n", suka_evenodd(words, len));

  if (suka_evenodd(words, len)) {
    for (i = 0; i < len; i++) {
      
      /* I love caching. */
      wlen = strlen(words[i]);

      /* If it's even sequence
         print it diagonally. */
      if (i % 2) {
        gap += wlen;
        printf("%*s\n", gap, words[i]);
      }
      /* Else, print horizontally. */
      else {
        for (ii = 0; ii < wlen; ii++) {
          gap++;
          printf("%*c\n", gap, words[i][ii]);
        }
      }

    }
  }
  else {
    for (i = 0; i < len; i++) {
      wlen = strlen(words[i]);

      switch (wlen % 4) {
        case 1:
          gap = strlen(words[i - 1]);
          break;
        case 2:
          gap = strlen(words[i + 1]);
          break;
        default:
          gap = 0;
      }
     
      printf("%d", gap);
      gap += strlen(words[i]);
      // printf("%d %s\n", (int)strlen(words[i - 1]), words[i]);
      // printf("%*s\n", gap, words[i]);
      suka_reverseprint(words[i], gap);
    }
  }
}


int suka_evenodd(char words[][16], int len) {
  int i;
  int even = 0;
  int odd = 0;

  for (i = 0; i < len; i++) {
    if (strlen(words[i]) % 2 == 0) {
      even++;
    }
    else {
      odd++;
    }
  }

  return odd > even;
}

/*

 .-------------------------.
(  Aku adalah anak gembala  )
 `-------------------------'


        ____    ,-.
       /   /)))( , )
      (  c'a(   \`'
      _) ) -/   |
      \_/ (_    |
      / \`~\\   |
     (,,,)  )) _j
      | /''((_/(_]
      \ \   `./ |             __  _
     ,'\ \_   `.|         .-.'  `; `-._  __  _
    /   `._\    \        (_,         .-:'  `; `-._
   /,,,      ,,,,\     ,'o"(        (_,           )
  /__|=     =\__\=\   (__,-'      ,'o"(            )>
 /'''',,,,   '```  \     (       (__,-'            )
/    =|_|=          \     `-'._.--._(             )
`-.__'"""`     ___.-'        |||  |||`-'._.--._.-'
  |(/`--....--'\ \           ''   ''    |||  |||
  |/           /\_\                     ''   ''
               `-. \  hjw & Bob Allison
                  `'
*/