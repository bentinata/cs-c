#include <stdio.h>
#include <string.h>
#define DIM 10

typedef struct {
  int x;
  int y;
} coord;

void dlom_fill(int arr[DIM][DIM], int filler, coord start, coord end);
