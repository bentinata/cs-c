#include <stdio.h>

int main() {
  int n;
  int i;
  int c;
  scanf("%d", &n);
  // n = 5;

  for (i = 0; i < n; ++i) {
    for (c = (i)? n - 1 - i: n; c > 0; printf(" ", c--)) {}
    for (c = (i)? n + 1 + i: n; c > 0; printf("*", c--)) {}
    for (c = n + i + 1; c > 0; printf(" ", c--)) {}
    for (c = n; c > 0; printf("*", c--)) {}
    printf("\n");
  }

  for (i = 0; i < n; ++i) {
    for (c = n + i; c > 0; printf(" ", c--)) {}
    for (c = n; c > 0; printf("*", c--)) {}
    for (c = n - i * 2; c > 0; printf(" ", c--)) {}
    for (c = n + ((n - i *2 < 0)? n - i * 2: 0); c > 0; printf("*", c--)) {}
    printf("\n");
  }

  for (i = 0; i < n; ++i) {
    for (c = n - i; c > 0; printf(" ", c--)) {}
    for (c = n; c > 0; printf("*", c--)) {}
    for (c = n + i * 2; c > 0; printf(" ", c--)) {}
    for (c = n; c > 0; printf("*", c--)) {}
    printf("\n");
  }

  return !"I don't like this case.";

}
/*

   ***    ***
 *****     ***
******      ***
   ***   ***
    *** ***
     *****
   ***   ***
  ***     ***
 ***       ***

*/
