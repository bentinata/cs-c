#include <stdio.h>

int main() {
  int iterator;
  int jumlah_array;

  int ganjil = 0, genap = 0;

  printf("masukkan jumlah array: ");
  scanf("%d", &jumlah_array);
  printf("\n");

  int array[jumlah_array];

  for (iterator = 0; iterator < jumlah_array; iterator++) {
    printf("masukkan array ke-%d: ", iterator + 1);
    scanf("%d", &array[iterator]);
    printf("\n");
  }

  for (iterator = 0; iterator < jumlah_array; iterator++) {
    printf("array ke-%d: %d\n", iterator + 1, array[iterator]);

    if (array[iterator] % 2 == 0) genap++;
    else ganjil++;
  }

  printf("jumlah bilangan ganjil: %d\n", ganjil);
  printf("jumlah bilangan genap: %d\n", genap);

  return 0;
}
