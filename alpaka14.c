#include <stdio.h>

int main() {
  char s[6], temp;
  int i,
      digit = 0,
      charac = 0,
      l = sizeof(s) / sizeof(s[0]);

  for (i = 0; i < l; i++) {
    scanf(" %c", &temp);

    if (isdigit(temp) == 1) {
      digit++;
      if (digit <= 3) {
        s[(digit * 2) - 1] = temp;

      }
    }
    else {
      charac++;
      if (charac <= 3) {
        s[(charac - 1) * 2] = temp;
      }
    }
  }

  if (digit > 3 || charac > 3) {
    printf("tidak valid\n");
  }
  else {
    for (i = 0; i < l; i++) {
      // printf("%d %d\n", i, l);
      printf("%c\n", s[i]);
    }
  }

  return 0;
}