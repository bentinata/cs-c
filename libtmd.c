#include "libtmd.h"

/* I had a problem with math.h pow() function.
   Since it's requires optional compile flag on my machine.
   I'd just implement simple integer-only power function. */
int ipow(int base, int exp)
{
    int result = 1;
    while (exp) {
        if (exp & 1) result *= base;
        exp >>= 1;
        base *= base;
    }

    return result;
}

/* printf() is bad, why don't use putchar while you can?
   This also don't use buffer since it will output per char
   instead of using char pointer.
   Cons: You can't format char. */
void putnchar(char x, int n) {
  while (n > 0) {
    putchar(x);
    n--;
  }
}

int tmd_conv(char *word, char *map[]) {
  int i;
  int nmap = 0; while (map[nmap] != 0) nmap++;

  for (i = 0; i < nmap; i++) {
    if (strcmp(word, map[i]) == 0) return i;
  }

  return -1;
}

void tmd_words(int idx[], char *map[]) {
  int i;
  int n = 0; while (idx[n] != -2) n++;

  for (i = 0; i < n - 1; i++) {
    if (idx[i] >= 0) printf("%s ", map[idx[i]]);
  }
  printf("%s\n", map[idx[n - 1]]);
}

void tmd_symbs(int idx[], int thick, int map[]) {
  putnchar('\n', thick);

  int n = 0; while (idx[n] != -2) n++;
  int nmap = 0; while (map[nmap] != 0) nmap++;
  /* We store whitespace so it wouldn't print unnecessary ' '. */
  int blank;
  int it, /* Thickness iterator. */
      i3, /* 3 char iterator. */
      ir, /* Row iterator. */
      is; /* Symbol iterator. */

  for (ir = 0; ir < 5; ir++) {

    for (it = 0; it < thick; it++) {
      blank = 0;

      for (is = 0; is < n; is++) {

        for (i3 = 0; i3 < 3; i3++) {

          /* Ah, bitwise operator.
             How many novice programmer don't know about you? */
          if (ipow(2, i3 + ir * 3) & map[idx[is]]) {
            putnchar(' ', blank);
            blank = 0;
            putnchar('0', thick);
          }
          else {
            blank += thick;
          }

        }

        blank += thick;

      }

      putchar('\n');

    }
  }

}

