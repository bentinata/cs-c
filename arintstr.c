////////////////////////////////////////////////////////////////////////////////
// Saya, Benget Nata, bersumpah untuk tidak melakukan kecurangan, yaitu:      //
// 1. Mengetikkan kode program dengan melihat kode program teman.             //
// 2. Mengetikkan kode program berdasarkan petunjuk oleh teman.               //
// 3. Mengumpulkan kode program milik teman.                                  //
// 4. Memberikan instruksi untuk mengetikkan kode program.                    //
// 5. Memiliki alur program yang sama persis dengan milik teman.              //
// Jika saya melakukan kecurangan maka Tuhan adalah saksi saya,               //
// dan saya bersedia menerima hukuman-Nya.                                    //
////////////////////////////////////////////////////////////////////////////////

/**
 * SWEETWARE LICENSE v0.1.1:
 * As long as you retain this notice you can do
 * whatever you want with this stuff.
 * If we meet some day, and you think this stuff is worth it,
 * you can buy me sweets in return.  Benget Nata
 */

#include "libarintstr.h"

int main() {

  int i; /* I, for one, welcome our iterator variable overlord. */
  int arrlen;
  scanf("%d", &arrlen);
  int arr[arrlen];

  for (i = 0; i < arrlen; i++) {
    scanf("%d", &arr[i]);
  }

  int arrofstrlen; /* Code Wars: A New Variable. */
  scanf("%d", &arrofstrlen);
  char arrofstr[arrofstrlen][16];

  for (i = 0; i < arrofstrlen; i++) {
    scanf("%s", arrofstr[i]);
  }

  /* Get min from first half of the array,
     and from second half of the array,
     then insert it inside variable in one go. */
  arrlen = max(
    arr_min(arr, 0, arrlen / 2 - 1),
    arr_min(arr, arrlen / 2, arrlen)
  );

  print_with_spec(arrofstr, arrofstrlen, arrlen);

  return 0;
}

/*
 .-------------------------.
(  Aku adalah anak gembala  )
 `-------------------------'


        ____    ,-.
       /   /)))( , )
      (  c'a(   \`'
      _) ) -/   |
      \_/ (_    |
      / \`~\\   |
     (,,,)  )) _j
      | /''((_/(_]
      \ \   `./ |             __  _
     ,'\ \_   `.|         .-.'  `; `-._  __  _
    /   `._\    \        (_,         .-:'  `; `-._
   /,,,      ,,,,\     ,'o"(        (_,           )
  /__|=     =\__\=\   (__,-'      ,'o"(            )>
 /'''',,,,   '```  \     (       (__,-'            )
/    =|_|=          \     `-'._.--._(             )
`-.__'"""`     ___.-'        |||  |||`-'._.--._.-'
  |(/`--....--'\ \           ''   ''    |||  |||
  |/           /\_\                     ''   ''
               `-. \  hjw & Bob Allison
                  `'
*/
