@ECHO OFF

REM This file is used in conjugation with C.sublime-build file.
REM Put this file anywhere within path.
REM Usually, I just put this inside mingw/bin.

cd %2
gcc %1.c -o %1.exe

IF %ERRORLEVEL% == 1 GOTO BUILDERR

%1.exe

:BUILDERR
ECHO.
PAUSE
EXIT