#include <stdio.h>
#include <limits.h> /* For INT_MAX usage. */


/* Typical comparison function.
   Would return either one is bigger.
   int a:  first value
   int b:  second value
*/
int max(int a, int b);

/* Typical comparison function.
   Would return either one is smaller.
   int a:  first value
   int b:  second value
*/
int min(int a, int b);

/* Array of integer comparator.
   Traverse the array from start, to end,
   and return any value that is smaller.
   int arr[]:   the would-be traversed array
   int start:   start traverse index
   int end:     end traverse index
*/
int arr_min(int arr[], int start, int end);

/* Custom print the array of string.
   Would print the value,
   then print word at index of the value,
   then print word at index of multiplication of the value.
   string words[]:  the suspect array
   int len:         array length, since c can't get array length reliably
   int val:         value for processing array
*/
void print_with_spec(char words[][16], int len, int val);
