#include <stdio.h>

int main() {
  int tanggal;
  int bulan;
  int tahun;
  int lama; // Unit = hari

  scanf("%d %d %d %d", &tanggal, &bulan, &tahun, &lama);
  
  tanggal = tanggal + lama;

  if (tanggal > 30) {
    bulan = bulan + tanggal / 30;
    tanggal = tanggal % 30;
  }

  if (bulan > 12) {
    tahun = tahun + bulan / 12;
    bulan = bulan % 12;
  }

  printf("%d ", tanggal);

  if (bulan == 1) {
    printf("Januari");
  }
  else if (bulan == 2) {
    printf("Februari");
  }
  else if (bulan == 3) {
    printf("Maret");
  }
  else if (bulan == 4) {
    printf("April");
  }
  else if (bulan == 5) {
    printf("Mei");
  }
  else if (bulan == 6) {
    printf("Juni");
  }
  else if (bulan == 7) {
    printf("Juli");
  }
  else if (bulan == 8) {
    printf("Agustus");
  }
  else if (bulan == 9) {
    printf("September");
  }
  else if (bulan == 10) {
    printf("Oktober");
  }
  else if (bulan == 11) {
    printf("November");
  }
  else if (bulan == 12) {
    printf("Desember");
  }

  printf(" %d", tahun);

  return 0;
}
