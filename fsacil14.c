#include <stdio.h>

int main() {
  double du[3], dub;
  int i, j,
      valid = 0,
      len = sizeof(du) / sizeof(du[0]),
      in[len];

  for (i = 0; i < len; i++) {
    scanf("%lf", &du[i]);
  }

  for (i = 0; i < len; i++) {
    scanf("%d", &in[i]);

    // Don't do checking if already valid.
    for (j = 0; j < len && valid < 2; j++) {
      if ((int)(((du[j] - (int)du[j])*100) + .5) % in[i] == 0 && (int)du[j] % in[i] == 0) {
        valid += 1;
      }

      // Don't continue checking if already valid.
      if (valid > 2) {
        break;
      }
    }

    // Reset checking if not valid.
    if (valid < 2) {
      valid = 0;
    }
  }

  if (valid < 2) {
    printf("tidak valid\n");
  }
  else {
    printf("valid\n");
  }

  return 0;
}