#include "lib.h"
#define rl(x) sizeof(x)/sizeof(x[0])

int main() {
  char m[8];
  int i, n, l, r, t, w;
  void (*sort)();

  scanf(" %s", m);

  scanf("%d %d %d", &n, &l, &r);
  int n1 = r - l + 1;
  int r1[n1];
  w = n1;

  for (i = 0; i < n; i++) {
    if (i >= l) scanf("%d", &r1[i - l]);
    else scanf("%d", &t);
  }

  scanf("%d %d %d", &n, &l, &r);
  int n2 = r - l + 1;
  int r2[n2];
  w += n2;

  for (i = 0; i < n; i++) {
    if (i >= l) scanf("%d", &r2[i - l]);
    else scanf("%d", &t);
  }

  int r_1_2[w];

  scanf("%d %d %d", &n, &l, &r);
  int n3 = r - l + 1;
  int r3[n3];
  w += n3;

  for (i = 0; i < n; i++) {
    if (i >= l) scanf("%d", &r3[i - l]);
    else scanf("%d", &t);
  }

  int rx[w];

  switch(m[0]) {
    case 's':
      sort = *sort_selection;
      break;
    case 'i':
      sort = *sort_insertion;
      break;
    case 'b':
      sort = *sort_bubble;
      break;
    case 'q':
      sort = *sort_bubble;
      break;
    default:
      break;
  }

  merge(sizeof(int), n1, r1, n2, r2, r_1_2, comp_int_d, sort);
  merge(sizeof(int), rl(r_1_2), r_1_2, n3, r3, rx, comp_int_d, sort);

  for (i = 0; i < w; i++)
    printf("%d%c", rx[i], i == w - 1 ? '\n' : ' ');

  return 0;
}
