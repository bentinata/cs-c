#include <stdio.h>

void fill(size_t s, char *x, char *d);

void swap(size_t s, char *l, char *r);

void sort_insertion(
  size_t s,
  size_t n,
  void *p,
  int (*c)(const void *, const void *));

void sort_bubble(
  size_t s,
  size_t n,
  void *p,
  int (*c)(const void *, const void *));

void sort_selection(
  size_t s,
  size_t n,
  void *p,
  int (*c)(const void *, const void *));

void merge(
  size_t s,
  size_t n1,
  void *p1,
  size_t n2,
  void *p2,
  void *px,
  int (*c)(const void *, const void *),
  void (*m)());

int comp_int_a(const void *_a, const void *_b);

int comp_int_d(const void *_a, const void *_b);
