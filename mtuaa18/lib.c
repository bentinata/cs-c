#include "lib.h"

void fill(size_t s, char *x, char *d) {
  size_t i;
  int a = *(const int*)x;
  //printf("filling %d...\n", a);

  for (i = 0; i < s; ++i, ++x, ++d) {
    *d = *x;
  }
}

void swap(size_t s, char *l, char *r) {
  size_t i;
  char t;

  for (i = 0; i < s; ++i, ++l, ++r) {
    t = *l;
    *l = *r;
    *r = t;
  }
}

void sort_insertion(
  size_t s,
  size_t n,
  void *p,
  int (*c)(const void *, const void *)) {
  char *l, *r, *a = (char *)p;

  for (l = a; l < a + n * s; l += s)
    for (r = l; r > a && c(r - s, r) > 0; r -= s) swap(s, r, r - s);
}

void sort_bubble(
  size_t s,
  size_t n,
  void *p,
  int (*c)(const void *, const void *)) {
  char *l, *r, *a = (char *)p;

  for (r = a + n * s; r > a; r -= s)
    for (l = a; l + s < r; l += s) if (c(l, l + s) > 0) swap(s, l, l + s);
}

void sort_selection(
  size_t s,
  size_t n,
  void *p,
  int (*c)(const void *, const void *)) {
  char *l, *r, *t, *a = (char *)p;

  for (l = a; l < a + n * s; l += s) {
    for (t = l, r = l + s; r < a + n * s; r += s) if (c(t, r) > 0) t = r;
    if (t != l) swap(s, l, t);
  }
}

void merge(
  size_t s,
  size_t n1,
  void *p1,
  size_t n2,
  void *p2,
  void *px,
  int (*c)(const void *, const void *),
  void (*m)()) {
  char *l1, *l2, *r1, *r2, *a1 = (char *)p1, *a2 = (char *)p2;
  char *ax = (char *)px;

  l1 = a1;
  r1 = a1 + n1 * s;
  l2 = a2;
  r2 = a2 + n2 * s;

  m(s, n2, p2, c);
  m(s, n1, p1, c);

  while (l1 < r1 && l2 < r2) {
    if (c(l1, l2) < 0) {
      fill(s, l1, ax);
      l1 += s;
    } else {
      fill(s, l2, ax);
      l2 += s;
    }
    ax += s;
  }

  if (l1 < r1)
    fill(r1 - l1, l1, ax);
  else
    fill(r2 - l2, l2, ax);

}

int comp_int_a(const void *_a, const void *_b) {
  int a = *(const int*)_a;
  int b = *(const int*)_b;
  return (a > b) - (a < b);
}

int comp_int_d(const void *_a, const void *_b) {
  int a = *(const int*)_a;
  int b = *(const int*)_b;
  return (a < b) - (a > b);
}
