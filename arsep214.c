#include <stdio.h>

/**
 * Summarize integer array from given start index until given end index.
 * @param  arr   One-level array.
 * @param  start Index for iterator to start. Minimum at 0.
 * @param  end   Index for iterator to end. Maximum at current array length.
 * @return       Integer of summarize.
 */
int arrsum(int *arr, int start, int end) {
  int sum = 0;

  while (start < end) {
    sum += arr[start];

    start++;
  }

  return sum;
}

int main() {
  int lenone, lentwo, i;

  scanf("%d", &lenone);

  int one[lenone];

  for (i = 0; i < lenone; i++) {
    scanf("%d", &one[i]);
  }

  scanf("%d", &lentwo);

  int two[lentwo];

  for (i = 0; i < lentwo; i++) {
    scanf("%d", &two[i]);
  }

  if (arrsum(one, 0, lenone / 2) == arrsum(two, lentwo / 2, lentwo)) {
    printf("valid\n");
  }
  else {
    printf("tidak valid\n");
  }

  return 0;
}