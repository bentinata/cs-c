////////////////////////////////////////////////////////////////////////////////
// Saya, Benget Nata, bersumpah untuk tidak melakukan kecurangan, yaitu:      //
// 1. Mengetikkan kode program dengan melihat kode program teman.             //
// 2. Mengetikkan kode program berdasarkan petunjuk oleh teman.               //
// 3. Mengumpulkan kode program milik teman.                                  //
// 4. Memberikan instruksi untuk mengetikkan kode program.                    //
// 5. Memiliki alur program yang sama persis dengan milik teman.              //
// Jika saya melakukan kecurangan maka Tuhan adalah saksi saya,               //
// dan saya bersedia menerima hukuman-Nya.                                    //
////////////////////////////////////////////////////////////////////////////////

/* SWEETWARE LICENSE v0.1.1:
 * As long as you retain this notice you can do
 * whatever you want with this stuff.
 * If we meet some day, and you think this stuff is worth it,
 * you can buy me sweets in return.  Benget Nata
 */

#include <stdio.h>

typedef struct food {
  int n; /* Numbered qualities. 0 = end; 1-5 = Bad; 6-10 = Good. */
  int m; /* Much price. */
  int i;
} food;

int main() {
  struct food foods[64]; /* I just like 64. */
  int  i = 1; /* Iterator. */
  char y; /* Yummy? E = Enak; S = Sedang. */
  char x; /* Xtra price? J = Mahal; K = Murah. */
  int  z; /* Ziah's money. */

  /* This will fill the foods array of struct.

     Successful scanf() returns greater-than-zero, thus will qualify as true.
     If input is wrong, scanf() will return 0. Two flies in one catch.
     And then checks if input value is greater-than-zero. */
  while (scanf("%d", &foods[i].n) && foods[i].n > 0) {
    scanf("%d", &foods[i].m);
    foods[i].i = i++;
  }

  /* Fill the checker with last data (index - 1),
     but set it's iteration variable to 0. */
  foods[0] = foods[--i];
  foods[0].i = 0;

  /* Now input Ziah's properties. (Preference and money.) */
  scanf(" %c %c", &y, &x);
  scanf("%d", &z);

  /* Iterate from behind. */
  while (i > 0) {
    /* If either Ziah's rating preference is fulfilled
       with current food rating, reuse and set it's food iterator variable to 1.
       If not set it to 0. */
    if ((y == 'E' && foods[i].m < z && foods[i].n > 5 && foods[i].n < 11)
      || (y == 'S' && foods[i].m < z && foods[i].n > 0 && foods[i].n < 6)) {
      foods[i].i = 1;
    } else {
      foods[i].i = 0;
    }

    /* Here the food iterator is used for checking current food validity
       with Ziah's rating preference.
       This design pattern is chosen so there's no nested loops.
       But, if nothing match, would not write anything to anywhere. */
    if ((x == 'J' && foods[i].i && foods[i].m >= foods[0].m)
      || (x == 'K' && foods[i].i && foods[i].m <= foods[0].m)) {
      foods[0] = foods[i];
    }

    /* Hey, make this loop works! */
    --i;
  }

  /* Since index-0 struct is set to 0 since it's inception,
     if any food is valid, it'd be changed. Otherwise no food is valid.*/
  if (foods[0].i == 0) {
    printf("Kelaparan\n");
  } else {

/*
           .--.    .-------------------------.
          /    \  | Mrs. Ziah, food's ready! |
         ## a  a  `.   .--------------------'
         (   '._)   \/
          |'-- |
        _.\___/_   ___food___
      ."\> \Y/|<'.  '._.-'
     /  \ \_\/ /  '-' /
     | --'\_/|/ |   _/
     |___.-' |  |`'`
       |     |  |
       |    / './
      /__./` | |
         \   | |
          \  | |
          ;  | |
          /  | |
    jgs  |___\_.\_
         `-"--'---'
*/

    printf("%d\n%d\n", foods[0].n, foods[0].m);
  }

  /* Variable i value is zero! Yay. :D */
  return i;
}
