#include "libbk13.h"

int div(int num) {
  return num < 1? 0: 1 + div(num / 2);
}
