#include "libarintstr.h"

void print_with_spec(char words[][16], int len, int val) {
  int i;
  printf("%d\n", val);

  for (i = 1; val * i < len; i++) {
    printf("%s\n", words[val * i -1]);
  }
}

int min(int a, int b) {
  return a < b? a: b;
}

int max(int a, int b) {
  return a > b? a: b;
}

int arr_min(int arr[], int start, int end) {
  int held;
  /* Held is set to INT_MAX, constant from limits.h
     so, each build of os would get the corresponding max int value.
     instead of hardcoding it to arbritary number. */
  for (held = INT_MAX; start < end; start++) {
    held = min(held, arr[start]);
  }

  return held;
}
