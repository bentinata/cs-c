#include "libdlom.h"
#define DIM 10

void dlom_fill(int arr[DIM][DIM], int filler, coord start, coord end) {
  //printf("filling map[%d][%d] to map[%d][%d]\n", start.x, start.y, end.x, end.y);
 
  // If start and end location is the same, don't loop over
  if (start.x == end.x && start.y == end.y) {
    arr[start.x][start.y] = filler;
  }
  // Else, just loop to fill coordinates
  else if (start.x == end.x) {
    if (start.y < end.y) {
      while (start.y < end.y) {
        start.y++;
        arr[start.x][start.y] = filler;
      }
    }
    else {
      while (start.y > end.y) {
        start.y--;
        arr[start.x][start.y] = filler;
      }
    }
  }
  else if (start.y == end.y) {
    if (start.x < end.x) {
      while (start.x < end.x) {
        start.x++;
        arr[start.x][start.y] = filler;
      }
    }
    else {
      while (start.x > end.x) {
        start.x--;
        arr[start.x][start.y] = filler;
      }
    }
  }
}
