#include <stdio.h>
#include <string.h>
#include <math.h>

int main() {
  char word[32];
  char excluse[32];

  scanf("%s", word);
  scanf("%s", excluse);
  // strcpy(word, "akukamudandia");
  // strcpy(excluse, "kamu");
  // strcpy(word, "1a2a3a4a5a6a7a8b9b");
  // strcpy(excluse, "a");

  int wlen = strlen(word);
  int xlen = strlen(excluse);
  int witr;
  int xitr;
  int match = 0;

  for (witr = 0; witr < wlen; witr++) {
    /* This would check if
      current word index plus excluse word times match
      is same as excluse index.

      Example, word is:

      abacad
      012345

      And excluse word is:

      ac
      01

      Now when word iterator hit 2, match would be found, and valued 1.
      It'll put index 2 same as
      current index + excluse wordlen * match
      2 + 2 * 1 = 4

      Therefore word index 2 is replaced by word index 4.
      And so on.

      */
    for (xitr = 0; word[witr + xlen * match + xitr] == excluse[xitr]; xitr++) {
      if (xitr == xlen -1) {
        match++;
      }
    }

    /* Might use conditional if here,
      so if witr == witr + xlen * match
      no char array assignment needed.

      BUT! MUH PRECIOUS BYTES! */
    // if (match)
    word[witr] = word[witr + xlen * match];
  }

  /* Reassign correct word length.
      Also reinitialize some unused variable for greater use. */
  wlen = strlen(word);
  witr = wlen / 2;

  printf("%s\n", word);
  for (xlen = 0; xlen < witr + 1; xlen++) {
    for (match = 0; match < xlen; match++) {printf(" ");}
    printf("%c", word[xlen]);
    for (match = 0; match < wlen - xlen * 2 - 2; match++) {printf(" ");}
/* If at the last index, just print once. */
    if (xlen != witr) printf("%c", word[wlen - xlen - 1]);
    printf("\n");
  }

  witr = sqrt(wlen);
  /* This is for checking round square root. */
  if (witr * witr != wlen) {
    witr++;
  }

  for (xlen = 0, xitr = 0; xlen < witr; xlen++) {
    for (match = 0; match < wlen / 2 - xlen; match++) {printf(" ");}
    for (match = 0; match < xlen * 2 + 1 && xitr < wlen; match++, xitr++) {
      /* Since we index current word, might as well use that. */
      printf("%c", word[xitr]);
    }
    printf("\n");
  }

  return 0;
}
