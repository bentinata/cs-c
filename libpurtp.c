#include "libpurtp.h"

char cardcmp(char a, char b) {
  if (a == 'A' || b == 'A') {
    return 'A';
  }
  else if (a == 'K' || b == 'K') {
    return 'K';
  }
  else if (a == 'Q' || b == 'Q') {
    return 'Q';
  }
  else if (a == 'J' || b == 'J') {
    return 'J';
  }
  else if (a == '1' || b == '1') {
    return '1';
  }
  else {
    return a > b? a: b;
  }
}

