#include <stdio.h>
#include <string.h>

char s[1000]; // String
int i = 0;  // Index

char vocal[] = "aeiou";
char consonant[] = "bcdfghjklmnpqrstvwxyz";
char numeric[] = "0123456789";
char terminator[] = {' ', '.', '\0'};

// Initial set
void setString() {
  scanf("%1000[^\n]s", &s);
}

char getCurrentChar() {
  return s[i];
}

int getCurrentIndex() {
  return i;
}

int isDone() {
  return getCurrentChar() == '.' || getCurrentChar() == '\0';
}

int isVocal(char c) {
  int j;

  for (j = 0; j < sizeof vocal / sizeof *vocal; j++) {
    if (getCurrentChar() == vocal[j]) return 1;
  }

  return 0;
}

int isConsonant(char c) {
  int j;

  for (j = 0; j < sizeof consonant / sizeof *consonant; j++) {
    if (getCurrentChar() == consonant[j]) return 1;
  }

  return 0;
}

int isNumeric(char c) {
  int j;

  for (j = 0; j < sizeof numeric / sizeof *numeric; j++) {
    if (getCurrentChar() == numeric[j]) return 1;
  }

  return 0;
}

int isTerminator(char c) {
  int j;

  for (j = 0; j < sizeof terminator / sizeof *terminator; j++) {
    if (getCurrentChar() == terminator[j]) return 1;
  }

  return 0;
}

int nextChar() {
  i += 1;
  return getCurrentIndex();
}

int nextWord() {
  nextChar();
  while (!isTerminator(getCurrentChar())) {
    nextChar();
  }
  nextChar();

  return getCurrentIndex();
}

int getCharType(char c) {
  if (isVocal(c)) return 1;
  if (isConsonant(c)) return 2;
  if (isNumeric(c)) return 3;
  if (isTerminator(c)) return -1;
  return 0;
}
