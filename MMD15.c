///////////////////////////////////////////////////////////////////
// Saya Benget Nata bersumpah tidak melakukan kecurangan, yaitu: //
// 1. Mengetikkan kode program dengan melihat kode program teman //
//    dalam kegiatan yang sama.                                  //
// 2. Mengetikkan kode program berdasarkan petunjuk oleh teman   //
//    dalam kegiatan yang sama.                                  //
// 3. Mengumpulkan kode program milik teman                      //
//    dalam kegiatan yang sama.                                  //
// 4. Memberikan instruksi untuk mengetikkan kode program        //
//    terkait kode program yang dikumpulkan dalam kegiatan.      //
// 5. Memiliki alur program yang sama persis dengan teman        //
//    dalam kegiatan yang sama.                                  //
//                                                               //
// Jika saya melakukan kecurangan maka Tuhan adalah saksi saya,  //
// dan saya bersedia menerima hukuman-Nya.                       //
///////////////////////////////////////////////////////////////////

/**
 * This code is licensed under Sweetware license.
 *
 * -----------------------------------------------------------------------------
 * SWEETWARE LICENSE v0.1.1:
 * As long as you retain this notice you can do
 * whatever you want with this stuff.
 * If we meet some day, and you think this stuff is worth it,
 * you can buy me sweets in return.  Benget Nata
 * -----------------------------------------------------------------------------
 *
 */

#include <stdio.h>
/* CONSTANT with UPPERCASE. */
#define MINUTE 60
#define HOUR (MINUTE * 60)
#define DAY (HOUR * 24)

/* CustomObject with UpperCamelCase. */
typedef struct {
  int h;
  int m;
  int s;
} Time;

int main() {
  /* Define every possible selection. */
  Time mobil;
  mobil.h = 7;
  mobil.m = 20;
  mobil.s = 33;

  Time bus;
  bus.h = 9;
  bus.m = 10;
  bus.s = 15;

  Time pesawat;
  pesawat.h = 2;
  pesawat.m = 40;
  pesawat.s = 10;

  Time selected;

  int vehicle;
  int weekday;
  int h;
  int m;
  int s;
  int start;
  int end;
  int dur;

  scanf("%d", &vehicle);
  scanf("%d", &weekday);
  scanf("%d %d %d", &h, &m, &s);

  switch (vehicle) {
  case 1:
    selected = mobil;
    printf("mobil\n");
    break;
  case 2:
    selected = bus;
    printf("bus\n");
    break;
  case 3:
    selected = pesawat;
    printf("pesawat\n");
    break;
  default:
    printf("pilihan tidak ada\n");
    /* Early return in-case of unwanted decision. */
    return 0;
  }

  start = h * HOUR + m * MINUTE + s;
  dur = selected.h * HOUR + selected.m * MINUTE + selected.s;
  end = start + dur;

  /* Get weekday. Since weekday wouldn't more than 7, we modulo it with 7. */
  weekday += end / DAY;
  weekday = weekday % 7;
  end = end % DAY;

  /* And hour. */
  h = end / HOUR;
  end = end % HOUR;
  m = end / MINUTE;
  s = end % MINUTE;

  printf("%d\n", weekday);
  printf("%d %d %.2d\n", h, m, s);

  return 0;
}
