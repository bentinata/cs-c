#include "lib.h"

int fib_n(int first, int second, int n) {
  return n == 1? first: fib_n(second, first + second, --n);
}

int scan() {
  int x;
  scanf("%d", &x);
  return x;
}

char whitespace(int x, int i) {
  return i % x < x - 1? ' ': '\n';
}

