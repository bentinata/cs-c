#include <stdio.h>

/*
 * Standard fibonacci function.
 * first  first number of sequence
 * second second number of sequence
 * n      desired nth of sequence
 * ←      nth number of sequence
 */
int fib_n(int first, int second, int n);

/*
 * Cheap scan. Return then immediately vanish.
 * ←  scan result
 */
int scan();

/*
 * Pretty formatting for matrices.
 * x  matrix width
 * i  current position of iterator in respect of width
 * ←  newline at the last indices of width, space if either
 */
char whitespace(int x, int i);
