#include "lib.h"

int main() {
  int w, h, iw, ih, b1, b2, x;

  scanf("%d %d %d %d", &w, &h, &b1, &b2);

  char m[w][h];

  printf("Matriks Lanjutan Fibonacci\n");
  for (iw = 0; iw < w; iw++)
    for (ih = 0; ih < h; ih++) {
      /* Add 3 because according to the assignment,
         the first element is the one after the second base. */
      x = fib_n(b1, b2, iw * w + ih + 3);
      m[iw][ih] = x > scan()? 'O': 'X';
      printf("%d%c", x, whitespace(h, ih));
    }

  printf("\nMatriks Hasil\n");
  for (iw = 0; iw < w; iw++)
    for (ih = 0; ih < h; ih++)
      printf("%c%c", m[iw][ih], whitespace(h, ih));

  return 0;
}
