////////////////////////////////////////////////////////////////////////////////
// Saya, Benget Nata, bersumpah untuk tidak melakukan kecurangan, yaitu:      //
// 1. Mengetikkan kode program dengan melihat kode program teman.             //
// 2. Mengetikkan kode program berdasarkan petunjuk oleh teman.               //
// 3. Mengumpulkan kode program milik teman.                                  //
// 4. Memberikan instruksi untuk mengetikkan kode program.                    //
// 5. Memiliki alur program yang sama persis dengan milik teman.              //
// Jika saya melakukan kecurangan maka Tuhan adalah saksi saya,               //
// dan saya bersedia menerima hukuman-Nya.                                    //
////////////////////////////////////////////////////////////////////////////////

/**
 * SWEETWARE LICENSE v0.1.1:
 * As long as you retain this notice you can do
 * whatever you want with this stuff.
 * If we meet some day, and you think this stuff is worth it,
 * you can buy me sweets in return.  Benget Nata
 */

#include <stdio.h>
#define _oaxh return

int main() {
  int theySaidIMustUse1DimensionArray[12] = {0};

  scanf("%d %d", &theySaidIMustUse1DimensionArray[0], &theySaidIMustUse1DimensionArray[1]);

  scanf("%d", &theySaidIMustUse1DimensionArray[2]);

  for (theySaidIMustUse1DimensionArray[3] = 0; theySaidIMustUse1DimensionArray[3] < theySaidIMustUse1DimensionArray[2]; theySaidIMustUse1DimensionArray[3]++) {
    scanf("%d", &theySaidIMustUse1DimensionArray[4]);

    for (theySaidIMustUse1DimensionArray[5] = 0;  theySaidIMustUse1DimensionArray[5] < theySaidIMustUse1DimensionArray[4]; theySaidIMustUse1DimensionArray[5]++) {
      scanf(" %c %d", &theySaidIMustUse1DimensionArray[6], &theySaidIMustUse1DimensionArray[7]);

      /* If first output is 'H' or 'h', it's horizontal movement.
         Either, it's vertical.
         Using both char, since using string.h isn't teached yet. */
      if (theySaidIMustUse1DimensionArray[6] == 'H' || theySaidIMustUse1DimensionArray[6] == 'h') {
        theySaidIMustUse1DimensionArray[0] += theySaidIMustUse1DimensionArray[7];
      /* Else if is failproof, better than just else. */
      } else if (theySaidIMustUse1DimensionArray[6] == 'V' || theySaidIMustUse1DimensionArray[6] == 'v') {
        theySaidIMustUse1DimensionArray[1] += theySaidIMustUse1DimensionArray[7];
      }
    }

    scanf("%d %d", &theySaidIMustUse1DimensionArray[8], &theySaidIMustUse1DimensionArray[9]);

    /* Check if snake's coordinate is same as fruit's coordinate. */
    if (theySaidIMustUse1DimensionArray[0] == theySaidIMustUse1DimensionArray[8] && theySaidIMustUse1DimensionArray[1] == theySaidIMustUse1DimensionArray[9]) {
      /* Real world start with one, while this program start with zero.
         Integer coercion is one, just append it. */
      printf("makanan %d\n", theySaidIMustUse1DimensionArray[3]+1);
      theySaidIMustUse1DimensionArray[10]++;
    }
  }

  /* Point is multiplication of ten. */
  printf("%d\n", theySaidIMustUse1DimensionArray[10] * 10);

                                         _oaxh 0;
                 /*.      .........   .u*"    ^Rc
                oP""*Lo*#"""""""""""7d" .d*N.   $
               @  u@""           .u*" o*"   #L  ?b
              @   "              " .d"  .d@@e$   ?b.
             8                    @*@me@#         '"Nu
            @                                        '#b
          .P                                           $r
        .@"                                  $L        $
      .@"                                   8"R      dP
   .d#"                                  .dP d"   .d#
  xP              .e                 .ud#"  dE.o@"(
  $             s*"              .u@*""     '""\dP"
  ?L  ..                    ..o@""        .$  uP
   #c:$"*u.             .u@*""$          uR .@"
    ?L$. '"""***Nc    x@""   @"         d" JP
     ^#$.        #L  .$     8"         d" d"
       '          "b.'$.   @"         $" 8"
                   '"*@$L $"         $  @
                   @L    $"         d" 8\
                   $$u.u$"         dF dF
                   $ """   o      dP xR
                   $      dFNu...@"  $
                   "N..   ?B ^"""   :R
                     """"* RL       d>
                            "$u.   .$
                              ^"*bo@"tony*/
}
