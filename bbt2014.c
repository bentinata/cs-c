#include <stdio.h>

int main() {
  int B,
      e,
      n,
      N;

  scanf("%d", &n);
  // n = 5;

  for (B = 1; B <= n * 2; B++) {
    for (e = B;             e > 0; printf("*", --e)) {}
    for (e = n * 4 - B * 2; e > 0; printf(" ", --e)) {}
    for (e = B * 2 - 1;     e > 0; printf("*", --e)) {}
    for (e = n * 4 - B * 2; e > 0; printf(" ", --e)) {}
    for (e = B;             e > 0; printf("*", --e)) {}
    printf("\n");
  }

  for (B = 1; B <= n; B++) {
    for (e = n * 2 + 1;     e > 0; printf(" ", --e)) {}
    for (e = n * 4 - 3;     e > 0; printf("*", --e)) {}
    printf("\n");
  }

  for (B = 0; B < n; B++) {
    for (e = n * 5 + B - 2; e > 0; printf(" ", --e)) {}
    for (e = n - B;         e > 0; printf("*", --e)) {}
    printf("\n");
  }

  N = !"is for anywhere, anytime along!";
  return N;
}

// *          *          *
// **        ***        **
// ***      *****      ***
// ****    *******    ****
// *****  *********  *****
// ***********************
//        *********
//        *********
//        *********
//              ***
//               **
//                *

// *              *              *
// **            ***            **
// ***          *****          ***
// ****        *******        ****
// *****      *********      *****
// ******    ***********    ******
// *******  *************  *******
// *******************************
//          *************
//          *************
//          *************
//          *************
//                   ****
//                    ***
//                     **
//                      *

// *                  *                  *
// **                ***                **
// ***              *****              ***
// ****            *******            ****
// *****          *********          *****
// ******        ***********        ******
// *******      *************      *******
// ********    ***************    ********
// *********  *****************  *********
// ***************************************
//            *****************
//            *****************
//            *****************
//            *****************
//            *****************
//                        *****
//                         ****
//                          ***
//                           **
//                            *
