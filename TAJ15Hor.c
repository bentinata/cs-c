///////////////////////////////////////////////////////////////////
// Saya Benget Nata bersumpah tidak melakukan kecurangan, yaitu: //
// 1. Mengetikkan kode program dengan melihat kode program teman //
//    dalam kegiatan yang sama.                                  //
// 2. Mengetikkan kode program berdasarkan petunjuk oleh teman   //
//    dalam kegiatan yang sama.                                  //
// 3. Mengumpulkan kode program milik teman                      //
//    dalam kegiatan yang sama.                                  //
// 4. Memberikan instruksi untuk mengetikkan kode program        //
//    terkait kode program yang dikumpulkan dalam kegiatan.      //
// 5. Memiliki alur program yang sama persis dengan teman        //
//    dalam kegiatan yang sama.                                  //
//                                                               //
// Jika saya melakukan kecurangan maka Tuhan adalah saksi saya,  //
// dan saya bersedia menerima hukuman-Nya.                       //
///////////////////////////////////////////////////////////////////

/**
 * This code is licensed under Sweetware license.
 *
 * -----------------------------------------------------------------------------
 * SWEETWARE LICENSE v0.1.1:
 * As long as you retain this notice you can do
 * whatever you want with this stuff.
 * If we meet some day, and you think this stuff is worth it,
 * you can buy me sweets in return.  Benget Nata
 * -----------------------------------------------------------------------------
 *
 */

#include <stdio.h>
#include <string.h>
#include <math.h>

/**
 * Basic array swap function.
 * @param  str Input word.
 * @return     Reversed input word.
 */
char* revstr(char *str) {
  char temp;
  int i,
      len = strlen(str) - 1,
      stop = len/2;

  for (i = 0; i < stop; i++, len--) {
    temp = str[i];
    str[i] = str[len];
    str[len] = temp;
  }

  return str;
}

/**
 * Power using normal iteration multiplication.
 * Unused since log(number) * power is faster.
 * @param  base  Number that will be multiplied.
 * @param  power The power itself.
 * @return       Number ^ power.
 */
int intpow(int base, int power) {
  int res;
  size_t i;

  for (i = 0, res = 1; i < power; i++){
    res *= base;
  }

  return res;
}

/**
 * Count the character of integer using mathemathical process.
 * Since the only one returned is the iteration counter.
 * For code didn't need to be filled.
 * Same function as strlen().
 * @param  value Number.
 * @return       Number character length.
 */
int intlen(int value) {
  int i;

  for(i = 0; value / (int)(pow(10, i) + .5) != 0 && i < 5; i++){}

  return i;
}

/**
 * Reverse integer using mathematical process.
 * The upside is integer process need less memory than string process.
 * @param  value Number.
 * @return       Characterically reversed number.
 */
int revint(int value) {
  int i, rev = 0, len = intlen(value), current;

  for (i = 0; i <= len; i++) {
    rev += value / (int)(pow(10, len - i) + .5) * (int)(pow(10, i - 1) + .5);
    value -= value / (int)(pow(10, len - i) + .5) * (int)(pow(10, len - i) + .5);
  }

  return rev;
}

/**
 * Create a H:i:s formatted time from integer.
 * @param  s Time in unit of seconds.
 * @return   H:i:s formatted time.
 */
char* sec_to_time(int s) {
  static char his_time[12];
  int h, i;

  h = s / 3600;
  s = s % 3600;

  i = s / 60;
  s = s % 60;

  sprintf(his_time, "%.2d:%.2d:%.2d", h, i, s);

  return his_time;
}

int main() {
  char year[4];
  int yeari;

  scanf("%4s", year);

  yeari = revint(strtol(year, 0, 10));

  printf("%d\n", yeari);
  printf("%s\n", sec_to_time(yeari));


  return 0;
}
