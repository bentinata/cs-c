////////////////////////////////////////////////////////////////////////////////
// Saya, Benget Nata, bersumpah untuk tidak melakukan kecurangan, yaitu:      //
// 1. Mengetikkan kode program dengan melihat kode program teman.             //
// 2. Mengetikkan kode program berdasarkan petunjuk oleh teman.               //
// 3. Mengumpulkan kode program milik teman.                                  //
// 4. Memberikan instruksi untuk mengetikkan kode program.                    //
// 5. Memiliki alur program yang sama persis dengan milik teman.              //
// Jika saya melakukan kecurangan maka Tuhan adalah saksi saya,               //
// dan saya bersedia menerima hukuman-Nya.                                    //
////////////////////////////////////////////////////////////////////////////////

/* SWEETWARE LICENSE v0.1.1:
 * As long as you retain this notice you can do
 * whatever you want with this stuff.
 * If we meet some day, and you think this stuff is worth it,
 * you can buy me sweets in return.  Benget Nata
 */

#include <stdio.h>
#include <string.h>



int main() {
  int len = 10;
  // int alllen;
  char words[len + 1][16];


  int templen = 0;
  
  while (scanf("%s", &words[0]) && strcmp("end", words[0])) {
    strcpy(words[++templen], words[0]);
    
  }
  
  if (templen < len) len = templen;


  int scrambler;
  int i, ii, iii; /* I love thiis kiind of variiable. */
  char scrm_init;
  char scrm_res;

  scanf("%d", &scrambler);
  
  if (scrambler > 0) {
    for (i = 0; i < scrambler; i++) {

      scanf(" %c %c", &scrm_init, &scrm_res);

      /* Begin traversing all words.
         
         Actually am kinda masochist
         since merging these word together
         would've been easier*/
      for (ii = 1; ii <= len; ii++) {
        
        /* Backwards or forward, does it matter? */
        for (iii = strlen(words[ii]) - 1; iii >= 0; iii--) {
          if (words[ii][iii] == scrm_init) words[ii][iii] = scrm_res;
        }
      }
    }
  }
  else {
    /* Secret feature begin. */
    
    phase1();
    phase2();
    phase3();
    
  }

  printf("here %d\n", len);
  for (i = 1; i <= len; i++) {
    printf("%s", words[i]);
    
    if (i < len) {
      printf(" ");
    }
    else {
      printf("\n");
    }
  }

  return 0;
}
