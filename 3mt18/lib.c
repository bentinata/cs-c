#include "lib.h"

int smst(size_t n, int r[n]) {
  int i, s = r[0];
  for (i = 1; i < n; i++) {
    if (r[i] < s)
      s = r[i];
  }

  return s;
}

/* We don't use x and y as VLAs to avoid read bleeding bigger array. */
void same(size_t s, int x, int y, int m[s][50][50]) {
  int ix, iy, found = 0;

  for (ix = 0; ix < x; ix++) {
    for (iy = 0; iy < y; iy++) {
      /* Compare all matrix and increment the found flag. */
      if (m[0][ix][iy] == m[1][ix][iy] &&
          m[1][ix][iy] == m[2][ix][iy] &&
          m[0][ix][iy] == m[2][ix][iy]) {
        printf("%d %d %d\n", m[0][ix][iy], ix, iy);
        found++;
      }
    }
  }

  if (!found)
    printf("tidak ada\n");
}

