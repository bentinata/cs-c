/*
Saya Benget Nata bersumpah tidak melakukan kecurangan, yaitu:
1. Mengetikkan kode program dengan melihat kode program teman
   dalam kegiatan yang sama.
2. Mengetikkan kode program berdasarkan petunjuk oleh teman
   dalam kegiatan yang sama.
3. Mengumpulkan kode program milik teman
   dalam kegiatan yang sama.
4. Memberikan instruksi untuk mengetikkan kode program
   terkait kode program yang dikumpulkan dalam kegiatan.
5. Memiliki alur program yang sama persis dengan teman
   dalam kegiatan yang sama.

Jika saya melakukan kecurangan maka Tuhan adalah saksi saya,
dan saya bersedia menerima hukuman-Nya.
*/

#include <stdio.h>

/*
 * Find the smallest integer from an array.
 * n  length of the passed array
 * r  the passed array
 * ←  the smallest array element
 */
int smst(size_t n, int r[n]);

/*
 * Print the same occurence.
 * Could be prettier than this, IMO.
 * s  number of matrix to compare
 * x  matrix width
 * y  matrix length
 * m  array of matrix to compare
 */
void same(size_t s, int x, int y, int m[s][50][50]);
