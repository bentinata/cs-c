#include "lib.h"
int main() {
  size_t n = 3;
  int i, ix, iy;
  int m[n][50][50];
  int x[n], y[n];

  /* Scan and fill matrix with iteration in one go. */
  for (i = 0; i < n; i++) {
    scanf("%d %d", &x[i], &y[i]);
    for (ix = 0; ix < x[i]; ix++) {
      for (iy = 0; iy < y[i]; iy++) {
        scanf("%d", &m[i][ix][iy]);
      }
    }
  }

  /* Only run with the smallest matrix slice
   * since we check all three matrix.
   */
  same(n, smst(n, x), smst(n, y), m);

  return 0;
}
