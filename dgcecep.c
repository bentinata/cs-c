#include <stdio.h>

int main() {
  int n;
  int child;
  int sell;
  int year;
  int sum;
  int i;

  scanf("%d", &n);
  scanf("%d", &child);
  scanf("%d", &sell);
  scanf("%d", &year);

  for (i = 1; i <= year; i++) {
    n += child * n / 2;
  }

  if (year > 2) {
    n -= (year - 2) * sell;
  }

  printf("%d\n", n);

  return 0;
}

// 2
// 4
// 16
//

// 4 4 2 2 36
// a
// a + (.5 * a * b) = a + ab/2
// a + (.5 * a * b) + .5 * (4 + (.5 * 4 * 4)) * 4
// 4 + 8 + .5 * 12 * 4
// a + ab/2 + .5 * a + (ab/2) * b
//
// .5 * 4 + 16