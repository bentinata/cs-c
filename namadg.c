#include <stdio.h>
#include <string.h>

int main() {
  /* Hardcode is teh way for fast solving. */
  const char *s[] = {"eioaaa", "sndmbtngks"};
  int         m,
              i,
              l[] = {strlen(s[0]), strlen(s[1]), 0},
              e;

  scanf("%d", &e);
  scanf("%d", &l[2]);
  for (m = 1; m <= e; m++) {
    for (i = 1; i <= 4; i++) {
      printf("%c", s[i%2][(m * i * l[2] - 1) % (l[i%2])]);
    }
    printf("\n");
  }

  return 0;
}
