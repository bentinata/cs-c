#include <stdio.h>

int dosomething(int (*fn)(int a, int b), int a, int b) {
  return fn(a, b);
}

int add(int a, int b) {
  return a + b;
}

int mul(int a, int b) {
  return a * b;
}

int main() {

  printf("%d", dosomething(add, 5, 2));

  return 0;
}

