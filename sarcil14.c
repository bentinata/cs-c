#include <stdio.h>

int main() {
  int i, j,
      big[3],
      len = sizeof(big) / sizeof(big[0]),
      small[len],
      valid = 0;

  for (i = 0; i < len; i++) {
    scanf("%d", &big[i]);
  }

  for (i = 0; i < len; i++) {
    scanf("%d", &small[i]);

    for (j = 0; j < len && valid < 2; j++) {
      if (big[j] % small[i] == 0) {
        valid += 1;
      }

      if (valid > 2) {
        break;
      }
    }

    if (valid < 2) {
      valid = 0;
    }
  }

  if (valid < 2) {
    printf("tidak valid\n");
  }
  else {
    printf("valid\n");
  }

  return 0;
}