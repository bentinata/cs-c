#include "libdlom.h"
#define DIM 10

int main() {
  int map[DIM][DIM];
  int i;
  int step;
  int movement;
  char direction[8];
  coord cursor;
  coord before_cursor;

  // I love oneliner
  memset(map, 0, sizeof map);

  // for (i = 0; i < DIM * DIM; i++) {
  //  map[i / DIM][i % DIM] = 0;
  //}

  scanf("%d %d", &cursor.x, &cursor.y);
  
  // Normalizing start coordinate
  cursor.x--;
  cursor.y--;
  dlom_fill(map, 1, cursor, cursor);

  scanf("%d", &step);

  while(step > 0) {
    // Save current cursor first
    before_cursor = cursor;
    scanf(" %s %d", direction, &movement);

    // Just need to check third character
    switch (direction[2]) {
      case 'r':

        // And then alter with relative movement
        cursor.x -= movement;
        movement = 2;
        break;
      case 'w':
        cursor.y += movement;
        movement = 3;
        break;
      case 'a':
        cursor.y -= movement;
        movement = 3;
        break;
      case 'n':
        cursor.x += movement;
        movement = 2;
        break;
      }
    
    dlom_fill(map, movement, before_cursor, cursor);
    step--;
  }

  // Single loop for two dimensional array using int-type coercion
  for (i = 0; i < DIM * DIM; i++) {
    switch (map[i % DIM][i / DIM]) {
      case 0:
        putchar('*');
        break;
      case 1:
        putchar('x');
        break;
      case 2:
        putchar('-');
        break;
      case 3:
        putchar('|');
        break;
    }

    // Newline every 10-th char
    if (i % DIM == 9) putchar('\n');
  }

  return 0;
} 
