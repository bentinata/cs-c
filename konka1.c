#include <stdio.h>
#include <ctype.h>

int main() {
  char in[6];
  int valid, i = 0, digit = 0;

  for (i = 0; i < 6; i++) {
    scanf(" %c", &in[i]);

    if (i > 0) {
      // Do relative checking 'cause it's more challenging
      if (isdigit(in[i-digit])) {
        valid++;
      }
      else {
        valid = -6;
      }
    }

    if (isdigit(in[i])) {
      digit = 1;
    }
    else {
      digit = 0;
    }
  }

  if (valid > 0) {
    printf("kombinasi valid\n");
  }
  else {
    printf("kombinasi tidak valid\n");
  }

  return 0;
}