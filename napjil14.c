#include <stdio.h>

int main() {
  int val[6],
      even = 0,
      odd = 0,
      i,
      l  = sizeof(val) / sizeof(val[0]),
      temp;

  for (i = 0; i < l; i++) {
    scanf(" %d",&temp);

    if (temp % 2 == 0) {
      even++;
      if (even <= 3) {
        val[(even - 1) * 2] = temp;
      }
    }
    else {
      odd++;

      if (odd <= 3) {
        val[(odd * 2) - 1] = temp;
      }
    }

  }


  if (odd > 3 || even > 3) {
    printf("tidak valid\n");
    return 0;
  }
  else {
    for (i = 0; i < l; i++) {
      printf("%d\n", val[i]);
    }
  }

  return 0;
}