////////////////////////////////////////////////////////////////////////////////
// Saya, Benget Nata, bersumpah untuk tidak melakukan kecurangan, yaitu:      //
// 1. Mengetikkan kode program dengan melihat kode program teman.             //
// 2. Mengetikkan kode program berdasarkan petunjuk oleh teman.               //
// 3. Mengumpulkan kode program milik teman.                                  //
// 4. Memberikan instruksi untuk mengetikkan kode program.                    //
// 5. Memiliki alur program yang sama persis dengan milik teman.              //
// Jika saya melakukan kecurangan maka Tuhan adalah saksi saya,               //
// dan saya bersedia menerima hukuman-Nya.                                    //
////////////////////////////////////////////////////////////////////////////////

/* SWEETWARE LICENSE v0.1.1:
 * As long as you retain this notice you can do
 * whatever you want with this stuff.
 * If we meet some day, and you think this stuff is worth it,
 * you can buy me sweets in return.  Benget Nata
 */

#include <stdio.h>
#include <string.h>

typedef struct {
  int kode;
  int harga;
  char nama[16];
} Menu;

void swap(size_t s, char *l, char *r) {
  size_t i;
  char t;

  for (i = 0; i < s; ++i, ++l, ++r) {
    t = *l;
    *l = *r;
    *r = t;
  }
}

void bub(size_t n, size_t s, void *p, int (*c)(const void *, const void *)) {
  char *l, *x, *r, *a = (char *)p;

  for (r = a + n * s; r > a; r -= s) {
    l = a;
    x = l + s;

    while (x < r) {
      if (c(l, x) > 0) {
        swap(s, l, x);
      } else {
        l += s;
        x += s;
      }
    }
  }
}

void find(size_t n, size_t s, void *p, int (*c)(const void *, const void *), const void *v) {
  char *l, *x, *r, *a = (char *)p;

  for (r = a + n * s; r > a; r -= s) {
    l = a;
    x = l + s;

    while (x < r) {
      if (c(l, v) == 0) {
        printf("%d %s %d\n", ((Menu *)l)->kode, ((Menu *)l)->nama, ((Menu *)l)->harga);
        //swap(s, l, x);
      } else {
        l += s;
        x += s;
      }
    }
  }
}

int compareMenuByNama(const void *a, const void *b) {
	return strcmp(((Menu *)a)->nama, ((Menu *)b)->nama);
}

int findMenuByKode(const void *a, const void *b) {
  return !(((Menu *)a)->kode == *((int *)b));
}

int findMenuByNama(const void *a, const void *b) {
  return strcmp(((Menu *)a)->nama, (char *)b);
}

int findMenuByHarga(const void *a, const void *b) {
  return !(((Menu *)a)->harga == *((int *)b));
}

int main() {
  int length;
  int i;

  char prop[10];
  int tint;
  char tstr[10];

  scanf("%d", &length);

  Menu menu[length];

  for (i = 0; i < length; i++) {
    scanf("%d %s %d", &menu[i].kode, &menu[i].nama, &menu[i].harga);
  }

	bub(length, sizeof menu[0], menu, *compareMenuByNama);

  //for (i = 0; i < length; i++) {
  //  printf("%d %s %d\n", menu[i].kode, menu[i].nama, menu[i].harga);
  //}

  scanf(" %s", &prop);
  if (strcmp(prop, "kode") == 0) {
    scanf("%d", &tint);
    find(length, sizeof menu[0], menu, *findMenuByKode, tint);
  } else if (strcmp(prop, "harga") == 0) {
    scanf("%d", &tint);
    find(length, sizeof menu[0], menu, *findMenuByHarga, tint);
  } else if (strcmp(prop, "nama") == 0) {
    scanf(" %s", &tstr);
    find(length, sizeof menu[0], menu, *findMenuByNama, tstr);
  }

  return 0;
}

