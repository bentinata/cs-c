#include <stdio.h>

/**
 * Summarize integer array from given start index until given end index.
 * @param  arr   One-level array.
 * @param  start Index for iterator to start. Minimum at 0.
 * @param  end   Index for iterator to end. Maximum at current array length.
 * @return       Integer of summarize.
 */
int arrsum(int *arr, int start, int end) {
  int sum = 0;

  while (start < end) {
    sum += arr[start];

    start++;
  }

  return sum;
}

int main() {
  char temp[8];
  int len, i;

  scanf("%d", &len);

  int arr[len];

  for (i = 0; i < len; i++) {
    scanf("%s", temp);
    arr[i] = strtol(temp, NULL, 2);
  }

  printf("%d\n", arrsum(arr, 0, len));

  return 0;
}