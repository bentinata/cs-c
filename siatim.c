#include <stdio.h>
#include <string.h>

int main() {
  char chunk[8];
  char babble[64];

  scanf("%s", chunk);
  scanf("%s", babble);
  // strcpy(chunk, "an");
  // strcpy(babble, "kebunbinataangbandungtamansari");

  int chunklen = strlen(chunk);
  int babblen = strlen(babble);
  int found = 0;
  int babble_iterator;
  int chunk_iterator;

  for (babble_iterator = 0; babble_iterator < babblen; babble_iterator++) {
    for (chunk_iterator = 0; chunk_iterator < chunklen; chunk_iterator++) {
      if (chunk[chunk_iterator] != babble[babble_iterator + chunk_iterator]) {
        break;
      } else if (chunk_iterator == chunklen - 1) {
        found++;
        babble_iterator += chunklen - 1;
      }
    }
  }

  printf("%d\n", found);

  return 0;
}
