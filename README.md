# What's this?

This is repository containing my experiment in C language, while finising excercise.
Instead of typical TDD, I'll try to keep code here as scalable as possible.

# Promise

Because every college excercise need to have a promise for agreeing with rules. I'll just put that here.

#### ID

Saya Benget Nata bersumpah tidak melakukan kecurangan, yaitu:
1. Mengetikkan kode program dengan melihat kode program teman dalam kegiatan yang sama.
2. Mengetikkan kode program berdasarkan petunjuk oleh teman dalam kegiatan yang sama.
3. Mengumpulkan kode program milik teman dalam kegiatan yang sama.
4. Memberikan instruksi untuk mengetikkan kode program terkait kode program yang dikumpulkan dalam kegiatan.
5. Memiliki alur program yang sama persis dengan teman dalam kegiatan yang sama.  
Jika saya melakukan kecurangan maka Tuhan adalah saksi saya, dan saya bersedia menerima hukuman-Nya.

#### EN-US

I, Benget Nata, swear for not cheating, as follows:
1. Type colleague source code in the case of event.
2. Type source code from dictation in the case of event.
3. Submit colleague source code in the case of event.
4. Dictate source code to colleague in the case of event.
5. Have the same exact program flow with colleague in the case of event.  
If I, somehow cheats, my swear is with God.