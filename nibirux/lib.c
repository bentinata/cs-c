#include "lib.h"

Weapon findWeapon(size_t s, Weapon *weapons, char *word) {
  int i;
  Weapon blank = { "", 0, 0 };
  for (i = 0; i < s; i++)
    if (strcmp(weapons[i].name, word) == 0)
      return weapons[i];

  return blank;
}

Enemy findEnemy(size_t s, Enemy *enemies, char *word) {
  int i;
  Enemy blank = { "", 0, 0, 0};

  for (i = 0; i < s; i++)
    if (strcmp(enemies[i].name, word) == 0)
      return enemies[i];

  return blank;
}
