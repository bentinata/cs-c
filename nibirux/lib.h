#include <stdlib.h>
#include <string.h>
#include <stdio.h>

typedef enum {
  FIRE,
  WATR,
  GRND,
  WIND,
} Element;

typedef struct {
  char *name;
  Element element;
  int damage;
} Weapon;

typedef struct Enemy {
  char *name;
  Element element;
  int damage;
  int hp;
} Enemy;

Weapon findWeapon(size_t s, Weapon *weapons, char *word);

Enemy findEnemy(size_t s, Enemy *enemies, char *word);
