#include "lib.h"

Weapon weapons[4] = {
  { "Buku_Bamp", FIRE, 500 },
	{ "Palu_Sidang_Tor", WATR, 125 },
	{ "Keyboard_Logitec", GRND, 150 },
	{ "Kipas_Angin_Sanyo", WIND, 50 },
};

Enemy enemies[4] = {
  { "Cacing_Besar_Alaska", FIRE, 200, 10000 },
  { "Siput_Gila", WATR, 350, 12500 },
	{ "Kunang_Kunang_Lembang", GRND, 150, 15000 },
	{ "Hasingin_Selayser", WIND, 500, 9000 },
};

int main() {
  char weaponname[32], enemyname[32];
  int strike, cycle, hp = 1250, totaldamage = 0;

  scanf("%s", weaponname);
  scanf("%d", &strike);
  scanf("%s", enemyname);
  scanf("%d", &cycle);

  Weapon weapon = findWeapon(4, weapons, weaponname);
  Enemy enemy = findEnemy(4, enemies, enemyname);

  while (cycle-- > 0 && hp && enemy.hp > totaldamage) {
    if (weapon.element == enemy.element)
      totaldamage += weapon.damage * strike;

    hp -= enemy.damage;
  }

  if (enemy.hp <= totaldamage) {
    printf("Kemenangan Untuk Bumi!\n");
    printf("Damage Wibi : %d\n", totaldamage);
  }
  else if (hp <= 0) {
    printf("Masih Ada Harapan!\n");
    printf("HP Monster : %d\n", enemy.hp - totaldamage);
  }
  else {
    printf("Pertarungan Belum Berakhir!\n");
    printf("Damage Wibi : %d\n", totaldamage);
    printf("HP Monster : %d\n", enemy.hp - totaldamage);
  }

  return 0;
}
