typedef struct {
  int length;
  size_t size;
  char *word;
} String;

String new(char *string, int length, size_t size) {
  return String = {*string, length, size};
}
