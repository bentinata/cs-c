////////////////////////////////////////////////////////////////////////////////
// Saya, Benget Nata, bersumpah untuk tidak melakukan kecurangan, yaitu:      //
// 1. Mentikkan kode program dengan melihat kode program teman.               //
// 2. Mentikkan kode program berdasarkan petunjuk oleh teman.                 //
// 3. Mengumpulkan kode program milik teman.                                  //
// 4. Memberikan instruksi untuk mengetikkan kode program.                    //
// 5. Memiliki alur program yang sama persis dengan milik teman.              //
// Jika saya melakukan kecurangan maka Tuhan adalah saksi saya,               //
// dan saya bersedia menerima hukuman-Nya.                                    //
////////////////////////////////////////////////////////////////////////////////

/**
 * SWEETWARE LICENSE v0.1.1:
 * As long as you retain this notice you can do
 * whatever you want with this stuff.
 * If we meet some day, and you think this stuff is worth it,
 * you can buy me sweets in return.  Benget Nata
 */


#include "libtmd.h"

int main() {

/*  Initialize data.  */
/*  We use multiple array since it has different type.
    Also we put end  marker at the bottom of the array
    to count array length later. */
  char *WORDS[] = {
    "tolong",
    "kirim",
    "berita",
    "kosong",
    "matamata",
    "pasukan",
    "aman",
    "masuk",
    "tahan",
    0
  };

  int SYMBS[] = {
    1488,
    11762,
    27556,
    11114,
    10922,
    15049,
    27520,
    14563,
    9617,
    -2
  };


  /* Obligatory, our beloved iterator operator. */
  int i;
  int len;
  int pos;
  int thickness;
  char word[16];

  scanf("%d", &len);
  /* We only need number list. */
  int in[len];
  for (i = 0; i < len; i++) {
    scanf("%s", word);
    /* Since we convert the string on-the-fly. */
    in[i] = tmd_conv(word, WORDS);
  }

  scanf("%d", &len);
  /* Must add extra space for array marker. */
  /* Point to consider:
     Add one element into array?
     Or add more function parameter?
     Since C don't support array length, and we pass decayed pointer. */
  int pick[len + 1];
  for (i = 0; i < len; i++) {
    scanf("%d", &pos);
    pick[i] = in[pos - 1];
  }

  /* The -2 part is arbritary.
     This can be filled by (-infinity, -1]
     Since (-1, infinity) is used. */
  pick[len] = -2;

  scanf("%d", &thickness);

  /*  Testing environtment. */
  // int pick[] = {0,1,2,3,4,5,6,7,8,-2};
  // int pick[] = {0,1,5,-2};
  // int pick[] = {0,-2};
  // int thickness = 2;

  /*  DO. THE. THING. */
  tmd_words(pick, WORDS);
  tmd_symbs(pick, thickness, SYMBS);

  return 0;
}
