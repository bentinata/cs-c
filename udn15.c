#include <stdio.h>
#include "libudn.h"

int main() {
  int n;
  int i;

  scanf("%d", &n);

  nilai daftar[n];

  for (i = 0; i < n; i++) {
    scanf(" %s %s %d", daftar[i].nama, daftar[i].kelas, &daftar[i].nilai);
  }

  sort_insertion(daftar, n);

  return 0;
}

/*

                  ___________
                 |||xxxxxxxxx|
                 |||xxxxxxxxx|
                 |||xxxxxxxxx|
                 |||xxxxxxxxx|
                 |||xxxxxxxxx|
                 |||xxxxxxxxx|
                 |||xxxxxxxxx|
                 ```~~~~~~~~~
 _________   _________   _________   _________
|A        | |5        | |3        | |2        |
|+   *    | |+        | |+   +    | |+        |
|    !    | |  +   +  | |         | |    +    |
|  *-+-*  | |    +    | |    +    | |         |
|    |    | |  +   +  | |         | |    +    |
|   ~~~  +| |        +| |    +   +| |        +|
|        V| |        S| |        E| |        Z|
 ~~~~~~~~~   ~~~~~~~~~   ~~~~~~~~~   ~~~~~~~~~
Hmm, I just need a 4+ for this to be straight flush.



                  ___________
                 |||xxxxxxx|p|
                 |||xxxxxxx|+|
                 |||xxxxxxx| |
                 |||xxxxxxx| |
                 |||xxxxxxx| |
                 |||xxxxxxx| |
                 |||xxxxxxx| |
                 ```~~~~~~~~~
  _________   _________   _________   _________
 |A        | |5        | |3        | |2        |
 |+   *    | |+        | |+   +    | |+        |
 |    !    | |  +   +  | |         | |    +    |
 |  *-+-*  | |    +    | |    +    | |         |
 |    |    | |  +   +  | |         | |    +    |
 |   ~~~  +| |        +| |    +   +| |        +|
 |        V| |        S| |        E| |        Z|
  ~~~~~~~~~   ~~~~~~~~~   ~~~~~~~~~   ~~~~~~~~~
 Whoa yes!!! A 4+!



                  ___________
                 |||xxxxxxxxx|
                 |||xxxxxxxxx|
                 |||xxxxxxxxx|
                 |||xxxxxxxxx|
                 |||xxxxxxxxx|
                 |||xxxxxxxxx|
                 |||xxxxxxxxx|
                 ```~~~~~~~~~
 ___ ___ ___ _________          _________
|A  |5  |3  |2        |        |4        |
|+  |+  |+  |+        |        |+    _   |
|   |  +|   |    +    |        |   _/ /_ |
|  *|   |   |         |        |  /_//_/ |
|   |  +|   |    +    |        |   /_/   |
|   |   |   |        +|        |        +|
|   |   |   |        Z|        |        b|
 ~~~ ~~~ ~~~ ~~~~~~~~~          ~~~~~~~~~
Wait...

 */
