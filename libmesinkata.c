#include "libmesinkata.h"

void STARTKATA(char string[]) {
  idx = 0;
  length = 0;

  while (string[idx] == ' ')
    idx++;

  while (string[idx] != ' ' && string[idx] != '.')
    word[length++] = string[idx++];

  word[length] = '\0';
}

void RESETKATA() {
  length = 0;
  word[0] = '\0';
}

void INCKATA(char string[]) {
  length = 0;

  while (string[idx] == ' ')
    idx++;

  while (string[idx] != ' ' && string[idx] != '.')
    word[length++] = string[idx++];

  word[length] = '\0';
}

char *GETCKATA() {
  return word;
}

int GETPANJANGKATA() {
  return length;
}

int EOPKATA(char string[]) {
  return string[idx] == '.';
}
