////////////////////////////////////////////////////////////////////////////////
// Saya, Benget Nata, bersumpah untuk tidak melakukan kecurangan, yaitu:      //
// 1. Mentikkan kode program dengan melihat kode program teman.               //
// 2. Mentikkan kode program berdasarkan petunjuk oleh teman.                 //
// 3. Mengumpulkan kode program milik teman.                                  //
// 4. Memberikan instruksi untuk mengetikkan kode program.                    //
// 5. Memiliki alur program yang sama persis dengan milik teman.              //
// Jika saya melakukan kecurangan maka Tuhan adalah saksi saya,               //
// dan saya bersedia menerima hukuman-Nya.                                    //
////////////////////////////////////////////////////////////////////////////////

/**
 * SWEETWARE LICENSE v0.1.1:
 * As long as you retain this notice you can do
 * whatever you want with this stuff.
 * If we meet some day, and you think this stuff is worth it,
 * you can buy me sweets in return.  Benget Nata
 */

#include <stdio.h>

#define MINHOUR 7
#define MAXHOUR 18

void intToDay(int day) {
  switch (day) {
    case 0:
      printf("Senin");
      break;
    case 1:
      printf("Selasa");
      break;
    case 2:
      printf("Rabu");
      break;
    case 3:
      printf("Kamis");
      break;
    case 4:
      printf("Jumat");
      break;
  }
}

int main() {
  int avail[5][MAXHOUR - MINHOUR] = {0};
  int n;
  int day;
  int start;
  int end;

  int plebs = 3;

  while (plebs) {
    scanf("%d", &n);

    while (n) {
      scanf("%d %d %d", &day, &start, &end);

      while (start < end) {
        avail[day - 1][start - MINHOUR] = 1;
        start += 1;
      }


      n -= 1;
    }

    plebs -= 1;
  }


  start = -1;
  for (day = 0; day < 5; day++) {
    for (end = 0; end < MAXHOUR - MINHOUR; end++) {
      if (avail[day][end] == 0 && start <= 0) {
        start = end + MINHOUR;
      }

      if (avail[day][end] == 1 && start > 0) {
        intToDay(day);
        printf(" %d.00 - %d.00\n", start, end + MINHOUR);
        start = 0;
      }

    }
    if (start > 0) {
      intToDay(day);
      printf(" %d.00 - %d.00\n", start, end + MINHOUR);
      start = 0;
    }
  }
  if (start == -1) {
    printf("Tidak ada irisan jadwal.\n");
  }

  return 0;
}
