////////////////////////////////////////////////////////////////////////////////
// Saya, Benget Nata, bersumpah untuk tidak melakukan kecurangan, yaitu:      //
// 1. Mentikkan kode program dengan melihat kode program teman.               //
// 2. Mentikkan kode program berdasarkan petunjuk oleh teman.                 //
// 3. Mengumpulkan kode program milik teman.                                  //
// 4. Memberikan instruksi untuk mengetikkan kode program.                    //
// 5. Memiliki alur program yang sama persis dengan milik teman.              //
// Jika saya melakukan kecurangan maka Tuhan adalah saksi saya,               //
// dan saya bersedia menerima hukuman-Nya.                                    //
////////////////////////////////////////////////////////////////////////////////

/**
 * SWEETWARE LICENSE v0.1.1:
 * As long as you retain this notice you can do
 * whatever you want with this stuff.
 * If we meet some day, and you think this stuff is worth it,
 * you can buy me sweets in return.  Benget Nata
 */

#include <stdio.h>
#include "libbk13.h"

int main() {
  int width, length;
  int i; // loop iterator

  scanf("%d", &width);
  scanf("%d", &length);

  // I hate multidimension array.
  // Don't write what you don't use.
  width *= length;
  int num[width];

  for (i = 0; i < width; i++) {
    scanf("%d", &num[i]);
  }

  for (i = 0; i < width; i++) {
    // Functional programming at it's best
    printf("(%d, %d)", num[i], div(num[i]));
    
    // People be like
    // "Start from zero"
    // But divider module be like
    // "Don't divide by zero"
    if ((i+1) % length)
      putchar(' ');
    else
      putchar('\n');
  }

  return 0;
}

// (-.- )
// ( -.-)
// (-.- )
// ( -.-)
// ( -.-)?  Where's my curves?
//
// ( o.o)!    )
//
//        )
// ( ^-^)/  Here it is!
