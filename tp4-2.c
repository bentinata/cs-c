#include <stdio.h>
#include <limits.h>

int main() {
  int n;
  int i;
  int x;

  int sum = 0;
  float avg;
  int min;

  scanf("%d", &n);

  scanf("%d", &x);
  sum = x;
  avg = x;
  min = x;

  for (i = 1; i < n; i++) {
    scanf("%d", &x);

    // Menghitung jumlah
    sum += x;

    // Menghitung rata-rata
    avg = sum/n;

    // Mencari minumum
    if (x < min)
      min = x;
  }

  printf("Sum: %d\nAvg: %.2f\nMin: %d", sum, avg, min);
  
  return 0;
}
