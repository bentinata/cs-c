#include <stdio.h>
#include <string.h>

typedef struct {
  char nama[16];
  char pekerjaan[16];
  int uang;

  int stamina;
  int energi;
  int sosial;
  int kesenangan;
  int jam;
} Karakter;

Karakter karakter;

void cls() {
  printf("Tekan keyboard untuk melanjutkan...\n");
  getch();
  system("cls");
}

void printbar(int num, char *bar) {
  printf("%-16s: ", bar);
  while(num--) {
    printf("|");
  }
  printf("\n");
}

void printjam() {
  int jam = karakter.jam / 100;
  int menit = karakter.jam - jam * 100;  

  printf("Jam\t\t: %d.%d", jam, menit);
}

void draw() {
  cls();
  printf("Nama\t\t: %s\n", karakter.nama);
  printf("Pekerjaan\t: %s\n", karakter.pekerjaan);
  printf("Uang\t\t: %d\n\n", karakter.uang);
  printf("STATUS\n");
  printbar(karakter.stamina, "Stamina");
  printbar(karakter.energi, "Energi");
  printbar(karakter.sosial, "Sosial");
  printbar(karakter.kesenangan, "Kesenangan");
  printf("\n\n");  
  printjam();
  printf("\n\n");
  printf("---------------------------------\n");
  printf("        Mini The Sims            \n");
  printf("---------------------------------\n");
  printf("1. Bersosial\n");
  printf("2. Belajar\n");
  printf("3. Makan\n");
  printf("4. Cari kerja\n");
  printf("5. Tidur\n");
  printf("6. Kerja\n");
  printf("7. Keluar\n");
} 

int password() {
  char password[32];
  int count = 3;  

  do {
    printf("Masukkan password: ");
    scanf("%s", &password);
    
    if (strcmp(password, "NAMAKU") == 0) return 0;
    
    if (count == 1) printf("Password sudah 3x salah\n");
    else printf("Password salah\n"); 
    
    cls();
  } while (--count);

  return 1;  
  
}

void nama() {
  printf("Masukkan nama: ");
  scanf("%s", karakter.nama);
}

void init() {
  strcpy(karakter.pekerjaan, "-");
  karakter.uang = 1000000;

  karakter.stamina = 9;
  karakter.energi = 9;
  karakter.sosial = 9;
  karakter.kesenangan = 9;
  karakter.jam = 900;
}

int main() {
  if (password()) return 0;
  
  init();
  nama();  
  draw();

  return 0;
}