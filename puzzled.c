#include <stdio.h>
#include <string.h>

char* strspin(char *str, int spin) {
  int i = 0;
  int len = strlen(str);

  if (spin < 0) {
    spin = len + spin;
  }
  if (spin > len) {
    spin = spin % len;
  }

  int mov = (i + spin) % len;
  char temp[2];
  temp[1] = str[0];

  while (i < len) {
    temp[0] = str[mov];
    str[mov] = temp[1];
    temp[1] = temp[0];

    i++;
    mov = (mov + spin) % len;
  }

  return str;
}

char* strmerge(char *str1, char *str2) {
  int i;
  int len = strlen(str1) + strlen(str2);
  char str[len];

  for (i = 0; i < len -1; i++) {
    str[i*2] = str1[i];
    str[i*2+1] = str2[i];
  }

  char *r = str;
  return r;
}

int main() {
  char one[32], two[32];
  int onespin, twospin;

  scanf("%s", one);
  scanf("%d", &onespin);
  scanf("%s", two);
  scanf("%d", &twospin);

  // strcpy(one, "senidombagarut");
  // onespin = 5;
  // strcpy(two, "senitangkasdgr");
  // twospin = 3;

  strcpy(one, strmerge(strspin(one, -onespin), strspin(two, twospin)));

  printf("%s\n", one);

  return 0;
}
