#include <stdio.h>
#include <time.h>
#include <string.h>
#include "sort.c"

// Struct size is 16 bit.
typedef struct {
  unsigned short int x;
  unsigned short int y;
  char name[4];
  short int e;
} Food;

// Struct size is 8 bit.
typedef struct {
  short int x;
  short int y;
} Step;

int w = 50;
int h = 20;
int e;
int nFood;
int nStep;
Food food[32]; // 64 byte
Step step[32]; // 32 byte

void wait(float x) {
  time_t start;
  time_t current;
  time(&start);
  do
    time(&current);
  while (difftime(current,start) < x);
}

// Compare y, if it's same, compare x.
int compareFood(const void *l, const void *r) {
  Food a = *((Food *)l);
  Food b = *((Food *)r);

  // if (strlen(a.name) == 0) return 1;
  return (strlen(a.name) == 0 > strlen(b.name) == 0) && (a.y == b.y ? a.x > b.x : a.y > b.y);
}

// Inserting foods data into in-program memory cache.
void readFood() {
  FILE *_food = fopen("tmakanan", "r");
  char temp[32]; // 4 byte

  nFood = 0;
  fscanf(_food, " %[^\n]s", &temp);

  while (temp[0] != '#') {
    sscanf(temp, "%d %d %s %d",
        &food[nFood].x, &food[nFood].y, &food[nFood].name, &food[nFood].e);
    fscanf(_food, " %[^\n]s", &temp);
    nFood++;
  }

  bub(nFood, sizeof(food[0]), food, *compareFood);
  fclose(_food);
  // for (i = 0; i < nFood; i++) printf("%d %d %d %s %d\n", i, food[i].x, food[i].y, food[i].name, food[i].e);
}

void writeFood() {
  FILE *_food = fopen("tmakanan", "w");
  int i;

  for (i = 0; i < nFood; i++) {
    if (strlen(food[i].name) > 0)
      fprintf(_food, "%d %d %s %d\n",
          food[i].x, food[i].y, food[i].name, food[i].e);
  }
  fprintf(_food, "#");
  fclose(_food);
}

void removeFood(int i) {
  Food empty = {0, 0, "", 0};
  food[i] = empty;
}

void addFood(Food f) {
  food[nFood].x = f.x;
  food[nFood].y = f.y;
  food[nFood].e = f.e;
  strcpy(food[nFood].name, f.name);
  nFood++;
}

// Inserting steps into cache. This doesn't need to be sorted.
void readStep() {
  FILE *_step = fopen("tperjalanan", "r");
  char temp[32]; // 4 byte

  nStep = 0;
  fscanf(_step, " %[^\n]s", &temp);

  while (temp[0] != '#') {
    sscanf(temp, "%d %d", &step[nStep].x, &step[nStep].y);
    fscanf(_step, " %[^\n]s", &temp);
    nStep++;
  }
  fclose(_step);
}

void writeStep() {
  FILE *_step = fopen("tperjalanan", "w");
  int i;

  for (i = 0; i < nStep; i++) {
    if (step[i].x >= 0)
      fprintf(_step, "%d %d\n", step[i].x, step[i].y);

  }
  fprintf(_step, "#");
  fclose(_step);
}

void removeStep(int i) {
  Step empty = {-1, -1};
  step[i] = empty;
}

void addStep(Step s) {
  step[nStep] = s;
  nStep++;
}

int putloop(int s, int c) {
  while (s--) putchar(c);
}

int putFood(int s, char name[], int indexA) {
  int i;
  for (i = 0; i <= s; i++) {
    if (i == indexA) putchar('A');
    else putchar(name[i]);
  }
  return s - 1;
}

void draw(int x, int y) {
  int iw;
  int ih;
  int iF = 0;
  int lF;

  for (ih = 0; ih <= h + 2; ih++) {
    for (iw = 0; iw <= w + 2; iw++) {
      if (iw == 0 || iw == w + 2) putchar('|');
      else if (ih == 0 || ih == h + 2) putchar('-');
      else if (food[iF].x + 1 == iw && food[iF].y + 1 == ih) {
        lF = strlen(food[iF].name);

        if (x == food[iF].x && y == food[iF].y) {
          e += food[iF].e;
          removeFood(iF);
          putchar('A');
        }
        else if (x > food[iF].x && x < food[iF].x + lF && y == food[iF].y)
          iw += putFood(lF, food[iF].name, x - food[iF].x);
        else
          iw += putFood(lF, food[iF].name, -1);

        iF++;
      }
      else if (x + 1 == iw && y + 1 == ih) putchar('A');
      else putchar(' ');
    }
    putchar('\n');
  }

  printf("\nEnergi A: %d\n", e);

}

void animate() {
  int i = 0;
  e = 0;

  while (i < nStep) {
    wait(0.03);
    system("clear");
    draw(step[i].x, step[i].y);
    i++;
  }
}

int menu() {
  int menu;

  printf("\nMenu:\n");
  printf("1. Panjang dan Lebar Papan\n");
  printf("2. Kelola makanan\n");
  printf("3. Kelola perjalanan\n");
  printf("4. A Cari Makan\n");
  printf("5. Keluar\n");
  printf("Masukkan Menu: ");
  scanf("%d", &menu);

  return menu;
}

int menuFood() {
  int menu;
  int i;
  char *label[] = {"x", "y", "nama", "energi"};

  readFood();

  printf("Isi makanan saat ini.\n");
  printf("-----------------------------\n");
  printf("| x  | y  | nama  | energi  |\n");
  printf("-----------------------------\n");
  for (i = 0; i < nFood; i++) {
    printf("|%4d|%4d|%7s|%9d|\n", food[i].x, food[i].y, food[i].name, food[i].e);
  }
  printf("-----------------------------\n");

  printf("\nMenu:\n");
  printf("1. Tambah\n");
  printf("2. Hapus\n");
  printf("3. Menu utama\n");
  printf("Masukkan Menu: ");
  scanf("%d", &menu);

  return menu;
}

Food scanFood() {
  Food f;

  printf("x: ");
  scanf("%d", &f.x);

  printf("y: ");
  scanf("%d", &f.y);

  printf("nama: ");
  scanf(" %s", &f.name);

  printf("energi: ");
  scanf("%d", &f.e);

  return f;
}

int scanFoodIndex() {
  int i;

  printf("index: ");
  scanf("%d", &i);

  return i - 1;
}

int menuStep() {
  int menu;
  int i;
  char *label[] = {"x", "y"};

  readStep();

  printf("Isi makanan saat ini.\n");
  printf("-----------\n");
  printf("| x  | y  |\n");
  printf("-----------\n");
  for (i = 0; i < nStep; i++) {
    printf("|%4d|%4d|\n", step[i].x, step[i].y);
  }
  printf("-----------\n");

  printf("\nMenu:\n");
  printf("1. Tambah\n");
  printf("2. Hapus\n");
  printf("3. Menu utama\n");
  printf("Masukkan Menu: ");
  scanf("%d", &menu);

  return menu;
}

Step scanStep() {
  Step s;

  printf("x: ");
  scanf("%d", &s.x);

  printf("y: ");
  scanf("%d", &s.y);

  return s;
}

int scanStepIndex() {
  int i;

  printf("index: ");
  scanf("%d", &i);

  return i - 1;
}

int scanWH() {
  printf("w: ");
  scanf("%d", &w);

  printf("h: ");
  scanf("%d", &h);
}

int main() {
  int choice;

  readFood();
  readStep();
  animate();
  // draw(8, 5);

  choice = menu();
  while (choice != 5) {
    switch(choice) {
      case 1:
        scanWH();
        break;
      case 2:
        choice = menuFood();
        while(choice != 3) {
          switch(choice) {
            case 1:
              addFood(scanFood());
              break;
            case 2:
              removeFood(scanFoodIndex());
              break;
          }
          writeFood();
          choice = menuFood();
        }
        break;
      case 3:
        choice = menuStep();
        while(choice != 3) {
          switch(choice) {
            case 1:
              addStep(scanStep());
              break;
            case 2:
              removeStep(scanStepIndex());
              break;
          }
          writeStep();
          choice = menuStep();
        }
        break;
      case 4:
        readFood();
        readStep();
        animate();
      break;
    }

    choice = menu();
  }

  return 0;

}
