 #include <stdio.h>
 #include <string.h>
 #include <math.h>

int main() {
  char input[128];
  int dimens;
  int len;
  int h_i;
  int w_i;

  scanf("%s", input);

  len = strlen(input);
  dimens = ceil(sqrt(len));

  for (h_i = 0; dimens > 0; h_i++, dimens--) {
    for (w_i = dimens - 1; w_i > 0; printf(" ", w_i--)) {}
    /*
      printf("n-%d: %d - %d\n", h_i, h_i * h_i, (h_i + 1) * (h_i + 1) - 1);
      n-1: 0
      n-2: 1 — 3
      n-3: 4 — 8
      f(n): n ^ 2 — (n + 1) ^ 2 - 1
    */
    for (w_i = h_i * h_i; w_i <= (h_i + 1) * (h_i + 1) - 1 && len > 0; printf("%c", input[w_i++]), len--) {}
    printf("\n");
  }

  return !"is Centaur's third skill.";
}
