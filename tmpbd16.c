#include <stdio.h>

// If the current char is '1'
// it will keep multiplied by 2
// while pushed to the right
// since we're evaluating binary
// from the leftmost position.
int parse(char *c) {
  int v = 0;

  while (*c) v += v + (*c++ == '1'? 1: 0);

  return v;
}

// Since managing strings is hard in C,
// I'll just print it out right here.
void print_binary(int v) {
  int n = 1;
  int i = 2;
  while (1 << n++ < v);

  char c[n];

  // printf("%d %d ", v, n);

  while (v) {
    *((char *)(c + n - i++)) = v % 2? '1': '0';
    v /= 2;
  }

  // n = 11 - sizeof c / sizeof *c;
  // while (n >= 0) {
  //   putchar('0');
  //   n--;
  // }

  printf("%s", c);
}

int main() {

  print_binary(parse("11111111111111111"));

  // char t[10];

  // // Smart people use dynamic length.
  // // Clever people use small, predefined length
  // // 'cause they know 150 bytes is'nt that much.
  // int d[3][50] = {{0}};
  // int l = 0;
  // int x;
  // int i;

  // for (i = 0; i < sizeof d / sizeof *d; i++) {
  //   scanf("%d", &d[i][0]);
  //   l += d[i][0];

  //   while (d[i][0]) {
  //     scanf("%s", &t);
  //     x = parse(t);
  //     print_binary(x);
  //     d[i][d[i][0]--] = x;
  //     // d[i][d[i][0]--] = parse(t);

  //   }
  // }


  //printf("%d", l);

}
